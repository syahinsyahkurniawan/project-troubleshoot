<svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23" fill="none">
  <g clip-path="url(#clip0_864_16760)">
    <path d="M18.0723 11.4375L20.1216 14.987" stroke="#505A60" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
    <path d="M13.8516 13.4102L14.4907 17.0348" stroke="#505A60" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
    <path d="M9.13924 13.4087L8.5 17.0339" stroke="#505A60" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
    <path d="M4.92445 11.4346L2.86523 15.0013" stroke="#505A60" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
    <path
      d="M2.875 9.42236C4.3855 11.2921 7.15463 13.6562 11.5001 13.6562C15.8455 13.6562 18.6146 11.2921 20.1251 9.42238"
      stroke="#505A60"
      stroke-width="1.5"
      stroke-linecap="round"
      stroke-linejoin="round"
    />
  </g>
  <defs>
    <clipPath id="clip0_864_16760">
      <rect width="23" height="23" fill="white" />
    </clipPath>
  </defs>
</svg>;
