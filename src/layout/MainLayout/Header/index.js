// material-ui
import { useTheme } from '@mui/material/styles';
import { Avatar, Box, Typography, useMediaQuery } from '@mui/material';

// project imports
import { LAYOUT_CONST } from 'constant';
import useConfig from 'hooks/useConfig';

import SearchSection from './SearchSection';

import ProfileSection from './ProfileSection';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'store';
import { openDrawer } from 'store/slices/menu';

// assets
import { IconMenu2 } from '@tabler/icons';
import Logo from 'components/ui-component/CompletedLogo';
import { useProfile } from 'hooks/queries/auth/useProfile';
import { useProfileData } from 'hooks/utils/useProfileData';
import { useEffect } from 'react';

// ==============================|| MAIN NAVBAR / HEADER ||============================== //

const Header = ({ titlePage, descPage }) => {
  const theme = useTheme();
  const { setUserData } = useProfileData();

  const dispatch = useDispatch();
  const { drawerOpen } = useSelector((state) => state.menu);

  const matchDownMd = useMediaQuery(theme.breakpoints.down('md'));
  const { layout } = useConfig();
  const { data } = useProfile();

  useEffect(() => {
    if (data) setUserData(data.data);
  }, [data]);

  return (
    <>
      {/* logo & toggler button */}
      <Box
        sx={{
          width: 228,
          display: 'flex',
          alignItems: 'center',
          [theme.breakpoints.down('md')]: {
            width: 'auto'
          }
        }}
      >
        <Box component="span" sx={{ display: { xs: 'none', md: 'block' }, flexGrow: 1 }}>
          <Logo />
        </Box>
        {(layout === LAYOUT_CONST.VERTICAL_LAYOUT || (layout === LAYOUT_CONST.HORIZONTAL_LAYOUT && matchDownMd)) && (
          <Avatar
            variant="rounded"
            sx={{
              ...theme.typography.commonAvatar,
              ...theme.typography.mediumAvatar,
              overflow: 'hidden',
              transition: 'all .2s ease-in-out',
              background: '#ffffff',
              color: '#A6A6A6',
              '&:hover': {
                background: '#2EABFF',
                color: '#ffffff'
              }
            }}
            onClick={() => dispatch(openDrawer(!drawerOpen))}
            color="inherit"
          >
            <IconMenu2 stroke={1.5} size="20px" />
          </Avatar>
        )}
      </Box>
      <Box sx={{ pl: { xs: 1, lg: 5 } }}>
        <Typography fontSize="20px" fontWeight={600} fontFamily="Inter">
          {titlePage}
        </Typography>
        <Typography fontSize="12px" fontFamily="Inter">
          {descPage}
        </Typography>
      </Box>
      <Box sx={{ flexGrow: 1 }} />
      {/* header search */}
      <SearchSection />
      <Box sx={{ flexGrow: 1 }} />

      <ProfileSection />

      {/* mobile header */}
      {/* <Box sx={{ display: { xs: 'block', sm: 'none' } }}>
        <MobileSection />
      </Box> */}
    </>
  );
};

Header.propTypes = {
  titlePage: PropTypes.string,
  descPage: PropTypes.string
};

export default Header;
