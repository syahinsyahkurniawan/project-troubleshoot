import styled from '@emotion/styled';
import { Box } from '@mui/system';
import Footer from 'components/home/footer';
import Header from 'components/home/header';
import Page from 'components/ui-component/Page';
import React from 'react';

const HeaderWrapper = styled('div')(({ theme }) => ({
  paddingTop: 30,
  overflowX: 'hidden',
  overflowY: 'clip',
  width: '100vw',
  background:
    theme.palette.mode === 'dark'
      ? theme.palette.background.default
      : `linear-gradient(360deg, ${theme.palette.grey[100]} 1.09%, ${theme.palette.background.paper} 100%)`,
  [theme.breakpoints.down('md')]: {}
}));
const SectionWrapper = styled(Box)({
  paddingTop: 100
});
const Container = styled('div')(({ theme }) => ({
  position: 'relative',
  display: 'flex',
  flexDirection: 'column',
  background: theme.palette.paper,
  overflowX: 'hidden'
}));
const LandingPageLayout = ({ children, title }) => (
  <Page title={title}>
    <HeaderWrapper>
      <Header />
    </HeaderWrapper>
    <Container>{children}</Container>
    <SectionWrapper
      sx={{
        backgroundImage: 'url(assets/images/landingpage/footer/footer-bg.png)',
        backgroundSize: { xs: 'contain', lg: 'cover' },
        backgroundRepeat: 'no-repeat',
        pt: { xs: '120px', md: '300px', xl: '400px' }
      }}
    >
      <Footer />
    </SectionWrapper>
  </Page>
);

export default LandingPageLayout;
