import styled from '@emotion/styled';

import PropTypes from 'prop-types';

const Div = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
  minHeight: '100vh',
  position: 'relative'
}));

const AuthLayout = ({ children }) => <Div>{children}</Div>;

AuthLayout.propTypes = {
  children: PropTypes.node
};
export default AuthLayout;
