import PropTypes from 'prop-types';

// project imports

// ==============================|| MINIMAL LAYOUT ||============================== //

const MinimalLayout = ({ children }) => (
  <>
    {children}
    {/* <Customization /> */}
  </>
);

MinimalLayout.propTypes = {
  children: PropTypes.node
};

export default MinimalLayout;
