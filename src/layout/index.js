import PropTypes from 'prop-types';

// project import
import LAYOUT from 'constant';
import AuthGuard from 'utils/route-guard/AuthGuard';
import GuestGuard from 'utils/route-guard/GuestGuard';
import MainLayout from './MainLayout';
import MinimalLayout from './MinimalLayout';
import AuthLayout from './AuthLayout';
import DashboardLayout from './DashboardLayout';

import LandingPageLayout from './LandingPageLayout';
// ==============================|| LAYOUTS - STRUCTURE ||============================== //

export default function Layout({ variant, children, titlePage = '', descPage = '' }) {
  switch (variant) {
    case LAYOUT.minimal:
      return <MinimalLayout>{children}</MinimalLayout>;

    case LAYOUT.noauth:
      return (
        <GuestGuard>
          <MinimalLayout>{children}</MinimalLayout>
        </GuestGuard>
      );

    case LAYOUT.authLayout:
      return <AuthLayout>{children}</AuthLayout>;

    case LAYOUT.dashboard:
      return <DashboardLayout>{children}</DashboardLayout>;

    case LAYOUT.landing:
      return <LandingPageLayout title={titlePage}>{children}</LandingPageLayout>;

    default:
      return (
        <AuthGuard>
          <MainLayout titlePage={titlePage} descPage={descPage}>
            {children}
          </MainLayout>
        </AuthGuard>
      );
  }
}

Layout.propTypes = {
  children: PropTypes.node,
  variant: PropTypes.string,
  titlePage: PropTypes.string,
  descPage: PropTypes.string
};
