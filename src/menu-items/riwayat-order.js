// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { IconTruckLoading } from '@tabler/icons';

// ==============================|| MENU ITEMS - riwayatOrder ||============================== //

const riwayatOrder = {
  id: 'Riwayat Order',
  title: <FormattedMessage id="riwayat-order" />,
  icon: IconTruckLoading,
  type: 'group',
  url: '/riwayat-order'
};

export default riwayatOrder;
