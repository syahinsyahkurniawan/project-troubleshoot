// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { IconTruckLoading } from '@tabler/icons';

// ==============================|| MENU ITEMS - alamatPickup ||============================== //

const alamatPickup = {
  id: 'Alamat Pickup',
  title: <FormattedMessage id="alamat-pickup" />,
  icon: IconTruckLoading,
  type: 'group',
  url: '/alamat-pickup'
};

export default alamatPickup;
