// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { IconReceipt2 } from '@tabler/icons';

// ==============================|| MENU ITEMS - pendapatan ||============================== //

const pendapatan = {
  id: 'Pendapatan',
  title: <FormattedMessage id="pendapatan" />,
  icon: IconReceipt2,
  type: 'group',
  url: '/pendapatan'
};

export default pendapatan;
