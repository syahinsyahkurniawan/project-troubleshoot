// third-party
import { FormattedMessage } from 'react-intl';

// assets
import { IconMapPin } from '@tabler/icons';

const icons = {
  IconMapPin
};

// ==============================|| MENU ITEMS - lacakOrder ||============================== //

const lacakOrder = {
  id: 'Lacak Order',
  title: <FormattedMessage id="lacak-order" />,
  icon: icons.IconMapPin,
  type: 'group',
  url: '/lacak-order'
};

export default lacakOrder;
