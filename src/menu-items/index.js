import alamatPickup from './alamat-pickup';
import dashboard from './dashboard';
// import application from './application';
// import forms from './forms';
// import elements from './elements';
// import samplePage from './sample-page';
// import pages from './pages';
// import utilities from './utilities';
// import support from './support';
// import other from './other';
import lacakOrder from './lacak-order';
import pendapatan from './pendapatan';
import riwayatOrder from './riwayat-order';
import tambahOrder from './tambah-order';
import userVerifikasi from './user-verifikasi';

// ==============================|| MENU ITEMS ||============================== //

const menuItems = {
  items: [dashboard, tambahOrder, lacakOrder, riwayatOrder, pendapatan, alamatPickup, userVerifikasi]
};

export default menuItems;
