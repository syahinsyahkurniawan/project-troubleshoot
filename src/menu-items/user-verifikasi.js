// third-party
import { IconUserSearch } from '@tabler/icons';
import { FormattedMessage } from 'react-intl';

// assets

// ==============================|| MENU ITEMS - userVerifikasi ||============================== //

const userVerifikasi = {
  id: 'User Verifikasi',
  title: <FormattedMessage id="user-verifikasi" />,
  icon: IconUserSearch,
  type: 'group',
  url: '/dashboard/user-verifikasi'
};

export default userVerifikasi;
