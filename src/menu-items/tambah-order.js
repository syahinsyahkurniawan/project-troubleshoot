// third-party
import { IconPlus, IconUserSearch } from '@tabler/icons';
import { FormattedMessage } from 'react-intl';

// assets

// ==============================|| MENU ITEMS - tambahOrder ||============================== //

const tambahOrder = {
  id: 'Tambah Order',
  title: <FormattedMessage id="tambah-order" />,
  icon: IconPlus,
  type: 'group',
  url: '/dashboard/tambah-order'
};

export default tambahOrder;
