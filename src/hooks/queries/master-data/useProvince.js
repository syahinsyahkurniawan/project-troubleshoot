import { useQuery } from '@tanstack/react-query';
import { getProvinceData } from 'api/master-data';

export const useProvince = () =>
  useQuery({
    queryKey: ['province'],
    queryFn: getProvinceData
  });
