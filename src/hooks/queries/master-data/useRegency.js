import { useQuery } from '@tanstack/react-query';
import { getRegencyData } from 'api/master-data';

export const useRegency = (data) =>
  useQuery({
    queryKey: ['regency', data],
    queryFn: () => getRegencyData(data)
  });
