import { useQuery } from '@tanstack/react-query';
import { getWilayahData } from 'api/master-data';

export const useWilayah = (data) =>
  useQuery({
    queryKey: ['wilayah', data],
    queryFn: () => getWilayahData(data)
  });
