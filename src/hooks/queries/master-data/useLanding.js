import { useQuery } from '@tanstack/react-query';
import { getLandingData } from 'api/landing';

export const useLanding = () =>
  useQuery({
    queryKey: ['landing'],
    queryFn: getLandingData
  });
