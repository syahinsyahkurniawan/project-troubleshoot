import { useQuery } from '@tanstack/react-query';
import { getDetailProfilesVerification } from 'api/dashboard/user-verifikasi';

export const useDetailUserVerifikasi = (uuid) => {
  useQuery({
    queryKey: ['ProfilesVerificationDetail', uuid],
    queryFn: () => getDetailProfilesVerification(uuid)
  });
};
