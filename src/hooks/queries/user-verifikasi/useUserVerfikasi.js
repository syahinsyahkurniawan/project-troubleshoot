import { useQuery } from '@tanstack/react-query';
import { getProfilesVerification } from 'api/dashboard/user-verifikasi';

export const useProfilesVerfication = () =>
  useQuery({
    queryKey: ['ProfilesVerfication'],
    queryFn: getProfilesVerification
  });
