import { useQuery } from '@tanstack/react-query';
import { getProfile } from 'api/auth';

export const useProfile = () =>
  useQuery({
    queryKey: ['profile'],
    queryFn: getProfile
  });
