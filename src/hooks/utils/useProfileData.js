import { useRouter } from 'next/router';
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { onLogout, setAuthData, setUserDetail } from 'store/slices/auth';

export const useProfileData = () => {
  const dispatch = useDispatch();
  const { data } = useSelector((s) => s?.profile);
  const router = useRouter();

  const setUserAuthData = useCallback(
    async (payload) => {
      dispatch(setAuthData(payload));
    },
    [dispatch]
  );
  const setUserData = useCallback(
    async (profile) => {
      dispatch(setUserDetail(profile));
    },
    [dispatch]
  );

  const onUserLogout = useCallback(() => {
    dispatch(onLogout());
    localStorage.clear();
    router.push('/');
  }, [dispatch]);

  return { data, setUserAuthData, setUserData, onUserLogout };
};
