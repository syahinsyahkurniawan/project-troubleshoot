/* eslint-disable consistent-return */
import { useEffect, useState } from 'react';

export const useCountDown = () => {
  const initialTime = 90; // 1 minute and 30 seconds
  const [time, setTime] = useState(initialTime);
  const [restart, setRestart] = useState(false);

  useEffect(() => {
    if (time > 0) {
      const timer = setInterval(() => {
        setTime((prevTime) => prevTime - 1);
      }, 1000);

      return () => {
        clearInterval(timer);
      };
    }
  }, [time]);

  const formatTime = (seconds) => {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
  };

  return { formatTime, time, restart, setRestart, setTime };
};
