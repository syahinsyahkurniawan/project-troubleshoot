export const useCheckJwt = () => {
  const jwt = localStorage.getItem('jwt') ?? '';
  const uuid = localStorage.getItem('uuid') ?? '';

  return { jwt, uuid };
};
