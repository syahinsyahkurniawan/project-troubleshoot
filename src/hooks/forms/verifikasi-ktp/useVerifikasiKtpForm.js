/* eslint-disable no-restricted-syntax */
import { yupResolver } from '@hookform/resolvers/yup';
import { useVerifikasiKtp } from 'hooks/mutation/verifikasi-ktp/useVerifikasiKtp';
import { useForm } from 'react-hook-form';
import { verifikasiKtpSchema } from 'schema/profile/verifikasi-ktp';

let verifikasiKtpData = null;
if (typeof localStorage !== 'undefined') {
  verifikasiKtpData = JSON.parse(localStorage.getItem('verifikasiKtpData'));
}

const defaultValues = {
  noKtp: verifikasiKtpData?.noKtp || '',
  nama: verifikasiKtpData?.nama || '',
  provinsiId: verifikasiKtpData?.provinsiId || 1,
  kabupatenId: verifikasiKtpData?.kabupatenId || 1,
  wilayahId: verifikasiKtpData?.wilayahId || 1,
  jenisKelamin: verifikasiKtpData?.jenisKelamin || '',
  alamat: verifikasiKtpData?.alamat || '',
  email: verifikasiKtpData?.email || '',
  hp: verifikasiKtpData?.hp || '',
  nama_bank: verifikasiKtpData?.nama_bank || '',
  nomor_rekening: verifikasiKtpData?.nomor_rekening || '',
  pemilik_rekening: verifikasiKtpData?.pemilik_rekening || '',
  ktp: {},
  photo: {},
  buku_rekening: {},
  agree: false
};

export const useVerifikasiKtpForm = () => {
  const form = useForm({ resolver: yupResolver(verifikasiKtpSchema), defaultValues });

  const verifikasiKtpHook = useVerifikasiKtp();
  const { mutate, isError } = verifikasiKtpHook;

  const onSubmit = form.handleSubmit(async (values) => {
    let profile;
    let userData;
    if (localStorage) {
      profile = JSON.parse(localStorage.getItem('completedprofile')).data;
      userData = JSON.parse(profile).detail;
      // console.log('completedprofile', JSON.parse(profile).detail, userData.email, userData.mobile);
    }

    const payload = new FormData();
    for (const [key, value] of Object.entries(values)) {
      payload.append(key, value);
    }
    payload.delete('ktp');
    payload.delete('email');
    payload.delete('hp');
    payload.delete('photo');
    payload.delete('agree');
    payload.append('userId', localStorage.getItem('userId'));
    payload.append('email', userData?.email || values.email);
    payload.append('handpone', userData?.mobile || values.hp);
    payload.append('isDraft', false);
    payload.append('ktp', values.ktp);
    payload.append('photo', values.photo);
    const uuid = localStorage.getItem('uuid');
    mutate({ uuid, payload });
  });

  return { ...form, onSubmit, isError };
};
