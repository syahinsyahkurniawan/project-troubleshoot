import { yupResolver } from '@hookform/resolvers/yup';

import { useRegister } from 'hooks/mutation/auth/useRegister';
import { useForm } from 'react-hook-form';

import { registerValidationScehma } from 'schema/auth/register';

const defaultValues = {
  fullname: '',
  email: '',
  mobile: '',
  password: '',
  passwordConfirm: '',
  agree: false
};

export const useRegisterForm = () => {
  const form = useForm({ resolver: yupResolver(registerValidationScehma), defaultValues });

  const regisHook = useRegister();
  const { mutate, isError, error } = regisHook;
  const onSubmit = form.handleSubmit(async (values) => {
    const payload = {
      ...values,
      mobile: `0${values.mobile}`
    };

    mutate(payload);
  });

  return { ...form, onSubmit, isError, error };
};
