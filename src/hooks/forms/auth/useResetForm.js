import { yupResolver } from '@hookform/resolvers/yup';

import { useReset } from 'hooks/mutation/auth/useReset';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { resetValidationScehma } from 'schema/auth/reset';

const defaultValues = {
  password: '',
  passwordConfirm: ''
};

export const useResetForm = () => {
  const router = useRouter();
  const {
    query: { uuid, token }
  } = router;
  const form = useForm({ resolver: yupResolver(resetValidationScehma), defaultValues });
  const { mutate } = useReset();

  if (!token || !uuid) router.push('/401');
  const onSubmit = form.handleSubmit(async (values) => {
    const payload = {
      ...values,
      token,
      uuid
    };
    console.log(payload);

    mutate(payload);
  });

  return { ...form, onSubmit };
};
