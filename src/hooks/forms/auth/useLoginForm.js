import { yupResolver } from '@hookform/resolvers/yup';

import { useForm } from 'react-hook-form';
import { loginValidationSchema } from 'schema/auth/login';

const defaultValues = {
  email: '',
  password: ''
};

export const useLoginForm = () => {
  const form = useForm({ resolver: yupResolver(loginValidationSchema), defaultValues });

  return { ...form };
};
