import { yupResolver } from '@hookform/resolvers/yup';
import { useForgot } from 'hooks/mutation/auth/useForgot';
import { useForm } from 'react-hook-form';
import { forgotValidationSchema } from 'schema/auth/forgot';

const defaultValues = {
  email: ''
};

export const useForgotForm = () => {
  const form = useForm({ resolver: yupResolver(forgotValidationSchema), defaultValues });

  const { mutate } = useForgot();

  const onSubmit = form.handleSubmit(async (values) => {
    mutate(values);
  });

  return { ...form, onSubmit };
};
