import { useResendOtp } from 'hooks/mutation/auth/useResendOTP';
import { useVerifyOTP } from 'hooks/mutation/auth/useVerifyOTP';
import { useRouter } from 'next/router';
import { useMemo, useState } from 'react';

export const useOtpForm = () => {
  const router = useRouter();
  const email = router?.query?.email;
  const [otp, setOtp] = useState();

  const { mutate, isError, isLoading } = useVerifyOTP();
  const { mutate: resendOtp, isSuccess, isLoading: resendLoading } = useResendOtp();

  const onChangeOtp = (e) => {
    setOtp(e);
  };
  const isDisabled = useMemo(() => {
    if (otp?.length > 0) return false;
    return true;
  }, [otp]);

  const onSubmit = async () => {
    const payload = {
      otpCode: +otp,
      email
    };

    mutate(payload);
  };

  const onResenOtp = async () => {
    resendOtp({ email });
  };

  return { otp, setOtp, onSubmit, isError, isDisabled, isLoading, onChangeOtp, onResenOtp, isSuccess, resendLoading };
};
