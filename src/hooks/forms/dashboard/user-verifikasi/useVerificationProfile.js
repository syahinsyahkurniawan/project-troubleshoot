import { yupResolver } from '@hookform/resolvers/yup';

import { useForm } from 'react-hook-form';
import { verificationProfileSchema } from 'schema/dashboard/user-verifikasi';

const defaultValues = {
  isVerification: '',
  keterangan: '-'
};

export const useVerificationProfileForm = () => {
  const form = useForm({ resolver: yupResolver(verificationProfileSchema), defaultValues });

  return { ...form };
};
