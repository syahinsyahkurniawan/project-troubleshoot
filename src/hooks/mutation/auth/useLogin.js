import { useMutation } from '@tanstack/react-query';
import { postLogin } from 'api/auth';

import { useProfileData } from 'hooks/utils/useProfileData';
import { useRouter } from 'next/router';
import { enqueueSnackbar } from 'notistack';
import { useCallback } from 'react';

export const useLogin = () => {
  const config = useConfig();

  return useMutation({
    mutationFn: postLogin,
    ...config
  });
};

const useConfig = () => {
  const { setUserAuthData, setUserData } = useProfileData();

  const router = useRouter();

  const onSuccess = useCallback(
    (response) => {
      const { data } = response;
      setUserAuthData(data).then(() => {
        localStorage.setItem('jwt', data?.access_token);
        localStorage.setItem('uuid', data?.user?.uuid);
        router.push('/dashboard');
      });
    },
    [router, setUserData]
  );
  const onError = useCallback(
    (error) => {
      if (error.response?.status === 400) {
        throw error;
      }
      if (error.response?.status >= 500) {
        return enqueueSnackbar('Terjadi kesalahan!', {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'center'
          },
          variant: 'error'
        });
      }
      throw error;
    },
    [enqueueSnackbar]
  );

  return { onError, onSuccess };
};
