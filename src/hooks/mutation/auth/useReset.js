import { useMutation } from '@tanstack/react-query';
import { postReset } from 'api/auth';
import { useRouter } from 'next/router';

import { enqueueSnackbar } from 'notistack';
import { useCallback } from 'react';

export const useReset = () => {
  const config = useConfig();

  return useMutation({
    mutationFn: postReset,
    ...config
  });
};

const useConfig = () => {
  const router = useRouter();
  const onSuccess = useCallback(() => {
    router.push('/auth/login');
    return enqueueSnackbar('Reset Password Berhasil! Silahkan Login!', {
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'center'
      },
      variant: 'success'
    });
  }, [router, enqueueSnackbar]);
  const onError = useCallback(
    (error) => {
      // console.log(error.response)
      if (error.response?.data?.statusCode === 400) {
        let dataMessage = error.response?.data?.message;
        let message = dataMessage.join(", ");
         return enqueueSnackbar(message, {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'center'
          },
          variant: 'error'
        });
      }
      if (error.response?.data?.statusCode === 403) {
         return enqueueSnackbar(error.response?.data?.message, {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'center'
          },
          variant: 'error'
        });
      }
      if (error.response?.status === 400) {
        throw error;
      }
      return enqueueSnackbar('Terjadi kesalahan!', {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center'
        },
        variant: 'error'
      });
    },
    [enqueueSnackbar]
  );

  return { onError, onSuccess };
};
