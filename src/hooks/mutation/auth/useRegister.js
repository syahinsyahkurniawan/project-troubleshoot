import { useMutation } from '@tanstack/react-query';
import { postRegistration } from 'api/auth';
import { useRouter } from 'next/router';
import { enqueueSnackbar } from 'notistack';
import { useCallback } from 'react';

export const useRegister = () => {
  const config = useConfig();

  return useMutation({
    mutationFn: postRegistration,
    ...config
  });
};

const useConfig = () => {
  const router = useRouter();
  const onSuccess = useCallback(
    (response) => {
      router.push(`/auth/register/otp?email=${response?.data?.email}`);
    },
    [router]
  );
  const onError = useCallback(
    (error) => {
      if (error.response?.status === 400) {
        throw error;
      }

      return enqueueSnackbar('Terjadi kesalahan!', {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center'
        },
        variant: 'error'
      });
    },
    [enqueueSnackbar]
  );

  return { onError, onSuccess };
};
