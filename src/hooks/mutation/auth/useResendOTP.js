import { useMutation } from '@tanstack/react-query';
import { resendOtp } from 'api/auth';
import { useRouter } from 'next/router';
import { enqueueSnackbar } from 'notistack';
import { useCallback } from 'react';

export const useResendOtp = () => {
  const config = useConfig();
  return useMutation({
    mutationFn: resendOtp,
    ...config
  });
};

const useConfig = () => {
  const router = useRouter();
  const onSuccess = useCallback(
    (data) =>
      enqueueSnackbar(data?.message, {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center'
        },
        variant: 'success'
      }),
    [router]
  );

  const onError = useCallback(
    (error) => {
      if (error.response?.status === 400) {
        throw error;
      }
      if (error.response?.status === 404) {
        return enqueueSnackbar('Otp Salah!', {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'center'
          },
          variant: 'error'
        });
      }

      return enqueueSnackbar('Terjadi kesalahan!', {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center'
        },
        variant: 'error'
      });
    },
    [enqueueSnackbar]
  );

  return { onError, onSuccess };
};
