import { useMutation } from '@tanstack/react-query';
import { postForgot } from 'api/auth';
import { enqueueSnackbar } from 'notistack';
import { useCallback } from 'react';

export const useForgot = () => {
  const config = useConfig();

  return useMutation({
    mutationFn: postForgot,
    ...config
  });
};

const useConfig = () => {
  const onSuccess = useCallback(
    () =>
      enqueueSnackbar('Silahkan cek email Anda!', {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center'
        },
        variant: 'success'
      }),
    [enqueueSnackbar]
  );
  const onError = useCallback(
    (error) => {
      if (error.response?.status === 400) {
        throw error;
      }
      if (error.response?.status === 401) {
        throw error;
      }
      if (error.response?.data?.statusCode === 400) {
        let dataMessage = error.response?.data?.message;
        let message = dataMessage.join(", ");
         return enqueueSnackbar(message, {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'center'
          },
          variant: 'error'
        });
      }
      if (error.response?.data?.statusCode === 403) {
         return enqueueSnackbar(error.response?.data?.message, {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'center'
          },
          variant: 'error'
        });
      }
      return enqueueSnackbar('Terjadi kesalahan!', {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center'
        },
        variant: 'error'
      });
    },
    [enqueueSnackbar]
  );

  return { onError, onSuccess };
};
