import { useMutation } from '@tanstack/react-query';
import { verifyOtp } from 'api/auth';
import { useRouter } from 'next/router';
import { enqueueSnackbar } from 'notistack';
import { useCallback } from 'react';

export const useVerifyOTP = () => {
  const config = useConfig();
  return useMutation({
    mutationFn: verifyOtp,
    ...config
  });
};

const useConfig = () => {
  const router = useRouter();
  const onSuccess = useCallback(() => {
    router.push('/auth/login');
    return enqueueSnackbar('Akun anda sudah Aktif!', {
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'center'
      },
      variant: 'success'
    });
  }, [router]);

  const onError = useCallback(
    (error) => {
      if (error.response?.status === 400) {
        throw error;
      }
      if (error.response?.status === 404) {
        return enqueueSnackbar('Otp Salah!', {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'center'
          },
          variant: 'error'
        });
      }
      if (error.response?.data?.statusCode === 400) {
        let dataMessage = error.response?.data?.message;
        let message = dataMessage.join(", ");
         return enqueueSnackbar(message, {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'center'
          },
          variant: 'error'
        });
      }
      if (error.response?.data?.statusCode === 403) {
         return enqueueSnackbar(error.response?.data?.message, {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'center'
          },
          variant: 'error'
        });
      }
      return enqueueSnackbar('Terjadi kesalahan!', {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center'
        },
        variant: 'error'
      });
    },
    [enqueueSnackbar]
  );

  return { onError, onSuccess };
};
