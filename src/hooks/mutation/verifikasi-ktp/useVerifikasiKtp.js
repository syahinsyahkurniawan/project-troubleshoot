import { useMutation } from '@tanstack/react-query';
import { insertProfile } from 'api/auth';
import { useRouter } from 'next/router';
import { enqueueSnackbar } from 'notistack';
import { useCallback } from 'react';

export const useVerifikasiKtp = () => {
  const config = useConfig();

  return useMutation({
    mutationFn: insertProfile,
    ...config
  });
};

const useConfig = () => {
  const router = useRouter();
  const onSuccess = useCallback(
    (data) => {
      // console.log('ktp res', data);
      router.push('/dashboard');
    },
    [router]
  );
  const onError = useCallback(
    (error) => {
      console.log(error.response?.status);
      if (error.response?.status === 400) {
        throw error;
      }
      if (error.response?.status === 401) {
        throw error;
      }

      return enqueueSnackbar('Terjadi kesalahan!', {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center'
        },
        variant: 'error'
      });
    },
    [enqueueSnackbar]
  );

  return { onError, onSuccess };
};
