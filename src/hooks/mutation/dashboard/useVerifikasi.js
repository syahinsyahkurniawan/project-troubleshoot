import { useMutation } from '@tanstack/react-query';
import { postLogin } from 'api/auth';
import { postVerificationUser } from 'api/dashboard/user-verifikasi';
import { useProfile } from 'hooks/queries/auth/useProfile';
import { useProfileData } from 'hooks/utils/useProfileData';
import { useRouter } from 'next/router';
import { enqueueSnackbar } from 'notistack';
import { useCallback } from 'react';

export const useVerifikasi = () => {
  const config = useConfig();

  return useMutation({
    mutationFn: postVerificationUser,
    ...config
  });
};

const useConfig = () => {
  const router = useRouter();

  const onSuccess = useCallback((response) => {
    const { data } = response;
    return enqueueSnackbar('Berhasil Memperbaharui Data', {
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'center'
      },
      variant: 'success'
    });
  }, []);
  const onError = useCallback(
    (error) => {
      console.log(error.response?.status);
      if (error.response?.status === 400) {
        throw error;
      }
      if (error.response?.status === 401) {
        throw error;
      }

      return enqueueSnackbar('Terjadi kesalahan!', {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center'
        },
        variant: 'error'
      });
    },
    [enqueueSnackbar]
  );

  return { onError, onSuccess };
};
