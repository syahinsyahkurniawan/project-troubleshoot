export const LAYOUT_CONST = {
  VERTICAL_LAYOUT: 'vertical',
  HORIZONTAL_LAYOUT: 'horizontal',
  DEFAULT_DRAWER: 'default',
  MINI_DRAWER: 'mini-drawer'
};

export const LAYOUT = {
  main: 'main',
  noauth: 'noauth',
  minimal: 'minimal',
  authLayout: 'auth',
  content: 'content',
  dashboard: 'dashboard',
  landing: 'landing'
};

export default LAYOUT;
