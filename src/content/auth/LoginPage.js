import { useTheme } from '@emotion/react';
import { Grid, useMediaQuery } from '@mui/material';

import Ilustration from 'components/auth/login/Ilustration';
import LoginForm from 'components/auth/login/LoginForm';
import RegisterNow from 'components/auth/login/RegisterNow';
import Welcome from 'components/auth/login/Welcome';
import AuthCardWrapper from 'components/authentication/AuthCardWrapper';
import Page from 'components/ui-component/Page';
import LAYOUT from 'constant';
import { useCheckJwt } from 'hooks/utils/useCheckJwt';

import Layout from 'layout';
import { useRouter } from 'next/router';
import React from 'react';

const LoginPageContent = () => {
  const { jwt } = useCheckJwt();
  const router = useRouter();
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
  if (jwt) {
    router.push('/dashboard');
  }
  return (
    <Page title="Login">
      <Grid item container alignItems="center" sx={{ minHeight: '100vh', width: '100vw' }}>
        <Ilustration />
        <Grid item container justifyContent="center" md={6} lg={7} sx={{ my: 3 }}>
          <AuthCardWrapper>
            <Grid container spacing={2} justifyContent="center">
              <Welcome matchDownSM={matchDownSM} theme={theme} />
              <LoginForm />
              <RegisterNow />
            </Grid>
          </AuthCardWrapper>
        </Grid>
      </Grid>
    </Page>
  );
};

LoginPageContent.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.authLayout}>{page}</Layout>;
};

export default LoginPageContent;
