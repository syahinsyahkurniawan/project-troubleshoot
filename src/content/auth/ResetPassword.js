// material-ui

import { Grid, Typography } from '@mui/material';

// project imports
import LAYOUT from 'constant';
import Layout from 'layout';
import Page from 'components/ui-component/Page';

import AuthCardWrapper from 'components/authentication/AuthCardWrapper';
import CompletedLogo from 'components/ui-component/CompletedLogo';
import AuthFooter from 'components/ui-component/cards/AuthFooter';

import { Box } from '@mui/system';
import IconLock from 'components/ui-component/IconLock';
import styled from '@emotion/styled';

import ResetForm from 'components/auth/reset-password/ResetForm';

const Container = styled('div')(({ theme }) => ({
  backgroundColor: '#F4F4F4',
  minHeight: '100vh',
  [theme.breakpoints.down('md')]: {
    padding: '0 20px'
  }
}));

const ResetPasswordConetent = () => (
  <Page title="Reset Password">
    <Container sx={{}}>
      <Grid container direction="column" justifyContent="space-between" sx={{ minHeight: '100vh', columnGap: '10px' }}>
        {/* <Grid container justifyContent="center" alignItems="center"> */}
        <Grid container item justifyContent="center" alignItems="center" xs={12} paddingTop="40px">
          <CompletedLogo />
        </Grid>
        <Grid item justifyContent="center" display="flex">
          <AuthCardWrapper sx={{ maxWidth: '500px' }}>
            <Grid container item alignItems="center" justifyContent="center" xs={12} sx={{ px: '25px' }} gap={2}>
              <Box
                alignItems="center"
                justifyContent="center"
                sx={{
                  mb: 3,
                  backgroundColor: '#F1FCFF',
                  borderRadius: '50%',
                  height: '84px',
                  width: '84px',
                  display: 'flex'
                }}
              >
                <IconLock />
              </Box>
              <Typography textAlign="center" variant="h4">
                Silahkan masukkan Password baru kamu. Pastikan sesuai dengan ketentuan buat password
              </Typography>
              <ResetForm />
            </Grid>
          </AuthCardWrapper>
        </Grid>

        <Grid item xs={12} padding="12px 0px">
          <AuthFooter />
        </Grid>
      </Grid>
    </Container>
  </Page>
);

ResetPasswordConetent.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.noauth}>{page}</Layout>;
};
export default ResetPasswordConetent;
