// material-ui
import { useTheme } from '@mui/material/styles';
import { Grid, Stack, Typography, useMediaQuery } from '@mui/material';

// project imports
import LAYOUT from 'constant';
import Layout from 'layout';
import Page from 'components/ui-component/Page';

import AuthCardWrapper from 'components/authentication/AuthCardWrapper';

import Ilustration from 'components/auth/login/Ilustration';
import RegisterForm from 'components/auth/register/RegisterForm';

const RegisterPage = () => {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <Page title="Register">
      <Grid container alignItems="center" sm={12} sx={{ minHeight: '100vh' }}>
        <Ilustration register />
        <Grid item container justifyContent="center" md={7} xl={7}>
          <AuthCardWrapper>
            <Grid container spacing={2} justifyContent="center">
              <Grid item xs={12}>
                <Grid
                  container
                  direction={matchDownSM ? 'column-reverse' : 'row'}
                  alignItems={matchDownSM ? 'center' : 'inherit'}
                  justifyContent={matchDownSM ? 'center' : 'space-between'}
                >
                  <Grid item>
                    <Stack justifyContent={matchDownSM ? 'center' : 'flex-start'} textAlign={matchDownSM ? 'center' : 'inherit'}>
                      <Typography color={theme.palette.grey[10]} gutterBottom variant={matchDownSM ? 'h3' : 'h2'}>
                        Selamat datang di Completed
                      </Typography>
                      <Typography color={theme.palette.grey[500]} variant="body1">
                        Isi data diri anda dan mulai bisnis anda dengan kami sekarang juga
                      </Typography>
                    </Stack>
                  </Grid>
                </Grid>
              </Grid>
              <RegisterForm />
            </Grid>
          </AuthCardWrapper>
        </Grid>
      </Grid>
    </Page>
  );
};

RegisterPage.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.authLayout}>{page}</Layout>;
};

export default RegisterPage;
