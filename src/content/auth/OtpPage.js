// material-ui
import { useTheme, styled } from '@mui/material/styles';
import { Grid, Stack, Typography, useMediaQuery } from '@mui/material';

// project imports
import LAYOUT from 'constant';
import Layout from 'layout';
import Page from 'components/ui-component/Page';

import { IconMail } from '@tabler/icons';
import { Box } from '@mui/system';
import Image from 'next/image';
import { useRouter } from 'next/router';
import OtpForm from 'components/auth/otp/OtpForm';

// assets

// styles
const Container = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
  minHeight: '100vh',
  position: 'relative',
  backgroundColor: '#F4F4F4',
  alignItems: 'center'
}));
const OtpContainer = styled(Grid)(() => ({
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
  height: '100%',
  position: 'relative',
  backgroundColor: '#fff',
  margin: 'auto',

  alignItems: 'center'
}));

const LogoCompleted = '/assets/completed-image/completed-logo.svg';

const OtpPage = () => {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
  const router = useRouter();

  const { email } = router?.query ?? '';
  if (!email) {
    router.push('/404');
  }

  return (
    <Page title="Code Verification">
      <Container justifyContent="space-between" alignItems="center" sx={{ backgroundColor: matchDownSM ? '#fff' : '#F4F4F4' }}>
        <Grid item container justifyContent="start" xs={12} sx={{ margin: 'auto' }}>
          <OtpContainer
            container
            sx={{
              minHeight: matchDownSM ? '100vh' : '600px',
              maxWidth: matchDownSM ? '100vw' : '584px',
              borderRadius: matchDownSM ? 0 : '8px',
              padding: matchDownSM ? '25px' : '40px',
              justifyContent: matchDownSM ? 'center' : 'flex-start',
              gap: '40px'
            }}
          >
            <Grid item container justifyContent="center">
              <Box style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Image src={LogoCompleted} alt="completed-logo" width={180} height={44} />
              </Box>
            </Grid>

            <Grid container spacing={2} justifyContent="center">
              <Grid item xs={12} justifyContent="center">
                <Grid container direction={matchDownSM ? 'column-reverse' : 'row'} alignItems="center" justifyContent="center">
                  <Stack justifyContent="center" textAlign="center" alignItems="center" gap={2}>
                    <Box
                      sx={{
                        borderRadius: '50%',
                        backgroundColor: 'primary.light',
                        height: '84px',
                        width: '84px',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center'
                      }}
                    >
                      <IconMail size={34} color={theme.palette.primary.main} />
                    </Box>
                    <Typography color="textPrimary" gutterBottom variant="h4">
                      Mohon Untuk Periksa Email Anda
                    </Typography>
                  </Stack>
                </Grid>
              </Grid>
              <Grid item>
                <Stack direction="row" justifyContent="center">
                  <Typography variant="caption" fontSize="0.875rem" textAlign="center">
                    Kami telah mengirimkan kode OTP verifikasi ke {email}
                  </Typography>
                </Stack>
              </Grid>
              <Grid container item xs={12}>
                <OtpForm />
              </Grid>
            </Grid>
          </OtpContainer>
        </Grid>
      </Container>
    </Page>
  );
};

OtpPage.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.authLayout}>{page}</Layout>;
};

export default OtpPage;
