import { useTheme } from '@emotion/react';
import { Grid, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { ContentWrapper } from 'components/home/container/ContainerContent';

const PrivacyPolicyContent = () => {
  const theme = useTheme();
  return (
    <>
      <Box
        sx={{
          background: theme.palette.primary.main,
          width: '100%',
          height: '394px',
          display: 'flex',
          alignItems: 'center',
          mx: 'auto'
        }}
      >
        <ContentWrapper style={{ alignItems: 'unset', gap: 'unset' }}>
          <Typography variant="h2" color={theme.palette.primary.contrastText} fontSize={{ md: '25px', xs: '18px' }}>
            Syarat dan Ketentuan
          </Typography>
          <Typography variant="h2" color={theme.palette.primary.contrastText} fontSize={{ md: '70px', xs: '48px' }}>
            COMPLETED
          </Typography>
        </ContentWrapper>
      </Box>
      <ContentWrapper>
        <Grid container gap={4} paddingTop={10}>
          <Grid item>
            <Box display="flex" flexDirection="column" gap="40px">
              <Typography variant="body3">Selamat datang di platform Completed</Typography>
              <Typography variant="body3">
                Syarat dan ketentuan di bawah ini mengatur penggunaan layanan jasa yang ditawarkan oleh PT. Completed Inovasi Teknologi
                terkait penyedia platform pengelola transaksi order barang dari mulai penginputan data barang, sampai dengan proses
                pengiriman ke pembeli. Syarat dan ketentuan ini dapat mempengaruhi hak dan kewajiban pengguna jasa Komerce dan di bawah
                ketentuan hukum yang berlaku.
              </Typography>
              <Typography variant="body3">
                Dengan menggunakan layanan kami, Anda dianggap telah membaca, mengerti, dan menyetujui serta terikat dan tunduk dengan
                segala syarat dan ketentuan yang berlaku di sini. Syarat dan ketentuan ini dapat diubah oleh kami dengan pemberitahuan
                terlebih dahulu.
              </Typography>
            </Box>
          </Grid>
          <Grid item>
            <Box display="flex" flexDirection="column" gap="40px">
              <Typography variant="h2">1. Definisi</Typography>
              <Box display="flex" flexDirection="column" gap="20px">
                <Typography variant="body3">
                  {`1.1 "Kami" : Adalah PT. Completed Inovasi Teknologi selaku pemilik Komerce dan pengelola platform Completed`}
                </Typography>
                <Typography variant="body3">{`1.2 "Anda" : Adalah orang yang mengakses situs atau pengguna platform Completed`}</Typography>
                <Typography variant="body3">{`1.3 "Partner" : Orang yang menggunakan jasa Completed`}</Typography>
                <Typography variant="body3">{`1.4 "Ekspedisi" : Perusahaan penyedia jasa pengiriman`}</Typography>
                <Typography variant="body3">{`1.5 "UMKM" : Bisnis yang dijalankan oleh perseorangan, rumah tangga, atau badan usaha yang memenuhi kriteria yang ditetapkan oleh Undang-Undang No. 20 tahun 2008`}</Typography>
                <Typography variant="body3">
                  1.6 Pebinsis online : Perusahaan atau perseorangan yang menjalankan bisnisnya melalui sistem digital atau elektronik
                </Typography>
                <Typography variant="body3">
                  {`1.7 "COD" : Cash on Delivery. Sistem pembelian atau transaksi yang pembayarannya dilakukan setelah barang diterima oleh pembeli`}
                </Typography>
                <Typography variant="body3">{`1.8 "Pickup" : Penjemputan barang ke toko penjual untuk diantarkan ke kantor ekspedis`}</Typography>
                <Typography variant="body3">{`1.9 "Data Pribadi" : Data-data pribadi Pengguna yang diberikan kepada pihak Komerce yang tidak terbatas pada meliputi nama, jenis kelamin, nomor telepon, alamat lengkap, informasi bisnis sebagaimana yang diminta dalam proses pemberdayaan Talent.`}</Typography>
              </Box>
            </Box>
          </Grid>
          <Grid item>
            <Box display="flex" flexDirection="column" gap="40px">
              <Typography variant="h2">2.Penggunaan & Syarat Ketentuan Platform</Typography>
              <Box display="flex" flexDirection="column" gap="20px">
                <Typography variant="body3">
                  2.1 Pengguna platform Completed merupakan individu atau badan usaha yang bergerak di bidang bisnis online baik
                  Business-to-Business maupun Business-to-Consumer (B2C).
                </Typography>
                <Typography variant="body3">
                  2.2 Pengguna platform Completed harus dapat memberikan informasi dan perusahaan dengan sebenar-benarnya.
                </Typography>
                <Typography variant="body3">
                  2.3 Apabila di kemudian hari ditemukan data yang tidak sesuai maka pihak Komerce berhak untuk mengakhiri kerjasama.
                </Typography>
                <Typography variant="body3">2.4 Pengguna platform Completed disebut Partner.</Typography>
              </Box>
            </Box>
          </Grid>
          <Grid item>
            <Box display="flex" flexDirection="column" gap="40px">
              <Typography variant="h2">3.Penggunaan Platform</Typography>
              <Box display="flex" flexDirection="column" gap="20px">
                <Typography>
                  Dengan menjadi pengguna platform Completed, maka Anda dianggap telah mampu untuk memenuhi syarat dan ketentuan sebagai
                  berikut:
                </Typography>
                <Typography variant="body3">
                  3.1 Anda tidak diperkenankan untuk menggunakan situs Completed, sebagian atau keseluruhan konten dari situs Completed
                  untuk keperluan pribadi. Hal ini dapat diartikan bahwa Anda hanya diperkenankan menggunakan situs, aplikasi, dan konten
                  dari Komship untuk keperluan individu atau bisnis yang menyediakan pengelolaan transaksi barang.
                </Typography>
                <Typography variant="body3">
                  3.2 Penyalahgunaan situs Completed untuk kepentingan pribadi maupun kelompok yang melanggar hukum akan ditindak sesuai
                  dengan peraturan yang diatur dalam Undang-Undang yang berlaku di Republik Indonesia.
                </Typography>
                <Typography variant="body3">
                  3.3 Pengguna jasa pengelola transaksi Completed yang terbukti menyalahgunakan kerjasama dengan Komerce untuk tindakan yang
                  melanggar hukum akan ditindak sesuai dengan peraturan perundang-undangan yang berlaku.
                </Typography>
                <Typography variant="body3">
                  3.4 Pengguna jasa dan situs Completed tidak diperkenankan untuk menggunakan situs dan/atau aplikasi untuk tindakan yang
                  mengganggu atau melanggar hak orang lain.
                </Typography>
                <Typography variant="body3">
                  3.5 Pengguna situs Completed tidak diperkenankan untuk mengubah data dan memodifikasi sebagian atau keseluruhan isi dari
                  situs maupun aplikasi tanpa persetujuan dari pihak Komship.
                </Typography>
              </Box>
            </Box>
          </Grid>
          <Grid item>
            <Box display="flex" flexDirection="column" gap="40px">
              <Typography variant="h2">4.Penyalahgunaan Platform</Typography>
              <Box display="flex" flexDirection="column" gap="20px">
                <Typography variant="body3">
                  4.1 Completed Berhak membekukan dan menindaklanjuti akun yang terbukti melakukan penarikan dan top-up tanpa transaksi
                  pengiriman yang jelas. Jika terjadi penarikan, maka akan dipotong oleh biaya admin.
                </Typography>
                <Typography variant="body3">
                  4.2 Completed berhak membekukan dan menindaklanjuti akun yang terbukti mencari keuntungan secara tidak wajar dengan
                  menyalahgunakan program Completed dengan cara seperti dengan membuat akun sebanyak-banyaknya, menyelesaikan misi untuk
                  mendapatkan reward gratis ongkir.
                </Typography>
              </Box>
            </Box>
          </Grid>
          <Grid item>
            <Box display="flex" flexDirection="column" gap="40px">
              <Typography variant="h2">5.Ketentuan Lainnya</Typography>
              <Box display="flex" flexDirection="column" gap="20px">
                <Typography>5.1 Completed berhak untuk merubah kebijakan di kemudian hari tanpa persetujuan pihak lain.</Typography>
                <Typography variant="body3">
                  5.2 Syarat-syarat yang tidak tertulis dalam dokumen ini bersifat fleksibel, kesepakatan dapat dicapai dengan diskusi yang
                  melibatkan kedua belah pihak.
                </Typography>
                <Typography variant="body3">
                  5.3 Untuk pertanyaan seputar hal-hal yang tidak disebutkan dalam Syarat dan Ketentuan ini, Anda dapat bertanya melalui
                  e-mail kami di kontak Completed atau menghubungi kami di nomor layanan Completed.
                </Typography>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </ContentWrapper>
    </>
  );
};

export default PrivacyPolicyContent;
