/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
import { useEffect, useState } from 'react';
// project imports
import Layout from 'layout';
import Page from 'components/ui-component/Page';
import LAYOUT from 'constant';
import UserVerifikasiTable from 'components/dashboard/user-verifikasi/tables';

const UserVerifikasiContent = () => (
  <Page title="UserVerifikasi Area">
    <UserVerifikasiTable />
  </Page>
);

export default UserVerifikasiContent;
