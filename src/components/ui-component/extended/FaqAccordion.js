import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import MuiAccordionSummary from '@mui/material/AccordionSummary';

// assets
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import AddIcon from '@mui/icons-material/Add';

// ==============================|| ACCORDION ||============================== //

const FaqAccordion = ({ data, defaultExpandedId = null, expandIcon, square, toggle }) => {
  const theme = useTheme();

  const [expanded, setExpanded] = useState(null);
  const handleChange = (panel) => (event, newExpanded) => {
    if (toggle) setExpanded(newExpanded ? panel : false);
  };

  const CustomExpandIcon = () => (
    <Box
      sx={{
        '.Mui-expanded & > .collapsIconWrapper': {
          display: 'none'
        },
        '.expandIconWrapper': {
          display: 'none'
        },
        '.Mui-expanded & > .expandIconWrapper': {
          display: 'block'
        }
      }}
    >
      <div className="expandIconWrapper">
        <RemoveCircleIcon sx={{ color: '#0068AE', height: '48px', width: '48px' }} />
      </div>
      <div className="collapsIconWrapper">
        <AddIcon sx={{ width: '48px', height: '48px' }} />
      </div>
    </Box>
  );
  useEffect(() => {
    setExpanded(defaultExpandedId);
  }, [defaultExpandedId]);

  return (
    <Box sx={{ width: '100%' }}>
      <MuiAccordion
        key={data.id}
        defaultExpanded={!data.disabled && data.defaultExpand}
        expanded={(!toggle && !data.disabled && data.expanded) || (toggle && expanded === data.id)}
        disabled={data.disabled}
        square={square}
        onChange={handleChange(data.id)}
      >
        <MuiAccordionSummary
          expandIcon={<CustomExpandIcon sx={{ width: '48px' }} />}
          sx={{ color: theme.palette.mode === 'dark' ? 'grey.600' : 'grey.900', fontWeight: 600, fontSize: '20px' }}
        >
          {data.name}
        </MuiAccordionSummary>
        <MuiAccordionDetails sx={{ fontSize: '18px', fontWeight: 400, paddingRight: '65px', textAlign: 'justify' }}>
          {data.description}
        </MuiAccordionDetails>
      </MuiAccordion>
    </Box>
  );
};

FaqAccordion.propTypes = {
  data: PropTypes.object,
  defaultExpandedId: PropTypes.string,
  expandIcon: PropTypes.object,
  square: PropTypes.bool,
  toggle: PropTypes.bool
};

export default FaqAccordion;
