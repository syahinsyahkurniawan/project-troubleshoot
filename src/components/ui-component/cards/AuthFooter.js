// material-ui
import { Link, Typography, Stack } from '@mui/material';

// ==============================|| FOOTER - AUTHENTICATION 2 & 3 ||============================== //

const AuthFooter = () => (
  <Stack container justifyContent="center" alignItems="center">
    <Typography variant="subtitle2" component={Link} href="https://completed.app" target="_blank" underline="hover">
      Copyright 2023 @ Completed
    </Typography>
  </Stack>
);

export default AuthFooter;
