import { Grid, List, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import MuiTypography from '@mui/material/Typography';
import PropTypes from 'prop-types';

import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { Box } from '@mui/system';

const KTPImageGuide = ({ title, image, points }) => (
  <Grid container sx={{ mb: 3 }}>
    <Grid item xs={12} sm={4}>
      <img src={image} alt="Contoh Gambar KTP" style={{ width: '90%' }} />
    </Grid>
    <Grid item xs={12} sm={8} display="flex" alignItems="center">
      <Box>
        <MuiTypography variant="h2" gutterBottom>
          {title}
        </MuiTypography>

        <List component="div" sx={{ mb: 3 }}>
          {points.map((p) => (
            <Box display="flex" alignItems="center" justifyContent="center">
              <ListItemIcon>
                <FiberManualRecordIcon sx={{ fontSize: '0.5rem' }} />
              </ListItemIcon>
              <ListItemText primary={p} />
            </Box>
          ))}
        </List>
      </Box>
    </Grid>
  </Grid>
);

KTPImageGuide.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  points: PropTypes.array
};

export default KTPImageGuide;
