/* eslint-disable @next/next/no-img-element */
import { Alert, Button, Grid, Typography } from '@mui/material';
import { Box, Stack } from '@mui/system';
import { useCallback, useState, useEffect } from 'react';
import { useDropzone } from 'react-dropzone';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { useVerifikasiKtpForm } from 'hooks/forms/verifikasi-ktp/useVerifikasiKtpForm';
import Image from 'next/image';

const KTPImageUpload = ({ name, setKtpImage }) => {
  const form = useVerifikasiKtpForm();
  const [selectedFile, setSelectedFile] = useState(null);

  useEffect(() => {
    const localStorageImage = localStorage.getItem(`verifikasiKtpImg${name}`);
    const localStorageImageName = localStorage.getItem(`verifikasiKtpImg${name}Name`);
    const localStorageImageSize = localStorage.getItem(`verifikasiKtpImg${name}Size`);
    setSelectedFile(localStorageImage);
    setSelectedFileSize(localStorageImageName);
    setSelectedFileName(localStorageImageSize);
  }, []);

  const handleFileChange = (e) => {
    const file = e[0];
    form.setValue('photo', file);
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        setSelectedFile(e.target.result);
        const base64String = e.target.result;
        localStorage.setItem(`verifikasiKtpImg${name}`, base64String);
        localStorage.setItem(`verifikasiKtpImg${name}Name`, file.name);
        localStorage.setItem(`verifikasiKtpImg${name}Size`, file.size);
      };
      reader.readAsDataURL(file);
      setSelectedFileName(file.name);
      setSelectedFileSize(file.size / 1024);
      setKtpImage(file);
    }
  };

  const [selectedFileSize, setSelectedFileSize] = useState();
  const [selectedFileName, setSelectedFileName] = useState();

  const onDrop = useCallback((acceptedFiles) => {
    handleFileChange(acceptedFiles);
  }, []);
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <>
      <Box
        sx={{
          width: '100%',
          border: '1px dashed #263238'
        }}
      >
        <Box
          {...getRootProps()}
          component="span"
          mt={3}
          py={10}
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          {!selectedFile && (
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <Image src="/assets/images/registration/upload-file.png" alt="Upload" width={100} height={100} />
              <Typography variant="subtitle1" my={3}>
                Cari File atau Drag Langsung ke dalam Box ini
              </Typography>
            </Box>
          )}
          <Controller
            name={name}
            control={form.control}
            render={() => <input type="file" onChange={handleFileChange} {...getInputProps()} />}
          />
          {!selectedFile && <Button variant="outlined">Pilih File</Button>}
          {selectedFile && <img src={selectedFile} alt="Thumbnail" style={{ maxWidth: '200px' }} />}
          {selectedFile && (
            <Stack direction="column" alignItems="center">
              <Typography align="center" variant="subtitle1" my={1}>
                {selectedFileName}
              </Typography>
              <Typography align="center" variant="subtitle3">
                {selectedFileSize} Kb
              </Typography>
            </Stack>
          )}
        </Box>
      </Box>
      <Grid container mt={2}>
        {selectedFile && (
          <Grid item sx={6} sm={3}>
            <Alert variant="filled" severity="success" sx={{ bgcolor: '#ECFFE8 !important', color: '#557A46' }}>
              Dokumen Sudah Sesuai
            </Alert>
          </Grid>
        )}
      </Grid>
    </>
  );
};

KTPImageUpload.propTypes = {
  setKtpImage: PropTypes.func
};

export default KTPImageUpload;
