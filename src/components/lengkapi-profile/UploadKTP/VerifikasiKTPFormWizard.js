/* eslint-disable @next/next/no-img-element */
// material-ui
import {
  Button,
  Step,
  Stepper,
  StepLabel,
  Stack,
  Typography,
  Grid,
  InputLabel,
  TextField,
  FormHelperText,
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
  Select,
  MenuItem,
  FormLabel,
  Checkbox,
  Alert,
  Autocomplete
} from '@mui/material';

// project imports
import MainCard from 'components/ui-component/cards/MainCard';
import AnimateButton from 'components/ui-component/extended/AnimateButton';
import { useVerifikasiKtpForm } from 'hooks/forms/verifikasi-ktp/useVerifikasiKtpForm';
import { Box } from '@mui/system';
import VerifikasiAlert from '../VerifikasiAlert';
import { Controller, useWatch } from 'react-hook-form';
import React, { useState, useEffect, useCallback } from 'react';
import { useProvince } from 'hooks/queries/master-data/useProvince';
import KTPImageGuide from './FormItems/KTPImageGuide';
import { useDropzone } from 'react-dropzone';
import Image from 'next/image';
import KTPImageUpload from './FormItems/KTPImageUpload';
import { useProfile } from 'hooks/queries/auth/useProfile';
import { useQuery } from '@tanstack/react-query';
import { getRegencyData, getWilayahData } from 'api/master-data';

// step options
const steps = ['Lengkapi Profile', 'Upload Foto KTP', 'Upload Foto Selfie dengan KTP', 'Upload Foto Buku Rekening'];

// ==============================|| FORMS WIZARD - BASIC ||============================== //

const VerifikasiKTPFormWizard = () => {
  const form = useVerifikasiKtpForm();
  const [activeStep, setActiveStep] = React.useState(0);

  const handleNext = () => {
    const formData = form.getValues();
    localStorage.setItem('verifikasiKtpData', JSON.stringify(formData));
    setActiveStep(activeStep + 1);
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };
  const { data: profileData } = useProfile();
  useEffect(() => {
    if (profileData) {
      localStorage.setItem('userId', profileData.data.id);
    }
  }, [profileData]);
  console.log('profileData', profileData);

  const [regencyQueryParams, setRegencyQueryParams] = useState({
    provinsiId: '',
    keyword: ''
  });
  const [wilayahQueryParams, setWilayahQueryParams] = useState({
    regencyId: '',
    keyword: ''
  });
  // STEP 1
  const { data: provinceData } = useProvince();

  const { data: regencyData } = useQuery({
    queryKey: ['regency', regencyQueryParams],
    queryFn: () => getRegencyData(regencyQueryParams)
  });
  const { data: wilayahData } = useQuery({
    queryKey: ['wilayah', wilayahQueryParams],
    queryFn: () => getWilayahData(wilayahQueryParams)
  });

  const selectedProvinsiId = useWatch({ control: form.control, name: 'provinsiId' });
  const selectedKabupatenId = useWatch({ control: form.control, name: 'kabupatenId' });
  const allFormValues = useWatch({ control: form.control });

  const [isDisableStepTwo, setIsDisableStepTwo] = useState(true);

  useEffect(() => {
    setIsDisableStepTwo(
      !allFormValues.noKtp || !allFormValues.nama || !allFormValues.alamat || !allFormValues.wilayahId || !allFormValues.agree
    );
  }, [allFormValues]);

  // Query After Select
  useEffect(() => {
    setRegencyKeyword('');
    setRegencyQueryParams({
      provinsiId: selectedProvinsiId,
      keyword: ''
    });
  }, [selectedProvinsiId]);
  useEffect(() => {
    setWilayahKeyword('');
    setWilayahQueryParams({
      regencyId: selectedKabupatenId,
      keyword: ''
    });
  }, [selectedKabupatenId]);
  const [regencyKeyword, setRegencyKeyword] = useState('');
  const [wilayahKeyword, setWilayahKeyword] = useState('');
  useEffect(() => {
    setWilayahQueryParams({
      regencyId: selectedKabupatenId,
      keyword: regencyKeyword
    });
  }, [regencyKeyword]);
  useEffect(() => {
    setWilayahQueryParams({
      regencyId: selectedKabupatenId,
      keyword: wilayahKeyword
    });
  }, [wilayahKeyword]);

  const [completedProfile, setCompletedProfile] = useState({
    email: '',
    hp: ''
  });
  useEffect(() => {
    if (localStorage) {
      const profile = JSON.parse(localStorage.getItem('completedprofile')).data;
      // console.log('ktp profile', profile, JSON.parse(profile).user);
      setCompletedProfile(JSON.parse(profile).detail);
    }
  }, []);

  // STEP 2
  const panduanFotoKTP = [
    'Foto dan data nomor KTP terbaca dengan jelas (Tidak blur, rusak atau tertutup jari/pantulan cahaya)',
    'no. KTP masih aktif.',
    'KTP milik sendiri. Sistem akan memerika kesesuain dengan from yang kamu isi sebelumnya.',
    'Format file .jpg, .jpeg atau .png'
  ];

  // STEP 3
  const panduanFotoSelfieKTP = [
    'ID tidak menutupi Muka dan Menghadap ke depan tidak menyamping',
    'Foto selfie dan ID terlihat dengan jelas (Tidak blur dan gelap)',
    'ID milik kamu sendiri (sama dengan nomor yang kamu masukkan sebelumnya)',
    'Format file .jpg, .jpeg atau .png',
    'Contoh foto selfie yang benar seperti yang terdapat pada gambar disamping.'
  ];

  const [selectedFile, setSelectedFile] = useState(null);
  const [selectedKtpSize, setSelectedKtpSize] = useState();
  const [selectedKtpName, setSelectedKtpName] = useState();

  const handleKtpChange = (e) => {
    console.log('form set value ktp', e, e[0]);
    const file = e[0];
    form.setValue('ktp', file);
    form.setValue('photo', file);
    console.log('form set value ktp', form.getValues());
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        setSelectedFile(e.target.result);
        const base64String = e.target.result;
        localStorage.setItem(`verifikasiKtpImgKtp`, base64String);
        localStorage.setItem(`verifikasiKtpImgKtpName`, file.name);
        localStorage.setItem(`verifikasiKtpImgKtpSize`, file.size);
      };
      reader.readAsDataURL(file);
      setSelectedKtpName(file.name);
      setSelectedKtpSize(file.size / 1024);
    }
  };

  const onDrop = useCallback((acceptedFiles) => {
    handleKtpChange(acceptedFiles);
  }, []);
  const { getRootProps: getRootPropsKtp, getInputProps: getInputPropsKtp } = useDropzone({ onDrop });

  return (
    <>
      <Stepper activeStep={activeStep} sx={{ pt: 3, pb: 5 }}>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>

      <form onSubmit={form.onSubmit} {...form} encType="multipart/form-data">
        <Box sx={{ display: activeStep === 0 ? 'block' : 'none' }}>
          <MainCard title="Detail Profile">
            <VerifikasiAlert />
            {/* <form onSubmit={form.onSubmit} {...form}> */}
            <Grid container rowGap={2} width="100%">
              <Grid item xs={12}>
                <Controller
                  name="noKtp"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Nomor KTP
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <TextField
                        fullWidth
                        placeholder="Masukkan no. KTP kamu"
                        error={form?.formState.errors.noKtp && Boolean(form?.formState?.errors?.noKtp)}
                        {...field}
                      />
                      {form?.formState.errors.noKTP && <FormHelperText error>Tolong masukkan no. KTP dengan sesuai</FormHelperText>}
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="nama"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Nama Lengkap Sesuai KTP
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <TextField
                        fullWidth
                        placeholder="Masukkan nama lengkap kamu"
                        error={form?.formState.errors.nama && Boolean(form?.formState?.errors?.nama)}
                        {...field}
                      />
                      {form?.formState.errors.nama && <FormHelperText error>Tolong masukkan no. KTP dengan sesuai</FormHelperText>}
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="jenisKelamin"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <FormControl>
                        <FormLabel id="gender">
                          Gender
                          <Typography component="span" color="red">
                            *
                          </Typography>
                        </FormLabel>
                        <RadioGroup
                          row
                          aria-labelledby="gender"
                          name="row-radio-buttons-group"
                          error={form?.formState.errors.jenisKelamin && Boolean(form?.formState?.errors?.jenisKelamin)}
                          {...field}
                        >
                          <FormControlLabel value="true" control={<Radio />} label="Laki-laki" />
                          <FormControlLabel value="false" control={<Radio />} label="Perempuan" />
                        </RadioGroup>
                      </FormControl>
                      {form?.formState.errors.jenisKelamin && <FormHelperText error>Tolong pilih jenis kelamin.</FormHelperText>}
                    </Box>
                  )}
                />
                {console.log('KTP FORM ERRORS: ', form?.formState.errors)}
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="hp"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        No. Handphone
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <TextField fullWidth {...field} value={completedProfile?.mobile} readonly disabled />
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="email"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Email
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <TextField fullWidth {...field} value={completedProfile?.email} readonly disabled />
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="alamat"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Alamat
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <TextField
                        fullWidth
                        multiline
                        minRows={5}
                        placeholder="Masukkan alamat sesuai KTP"
                        error={form?.formState.errors.alamat && Boolean(form?.formState?.errors?.alamat)}
                        {...field}
                      />
                      {form?.formState.errors.alamat && <FormHelperText error>Tolong masukkan alamat</FormHelperText>}
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="provinsiId"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Provinsi
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <FormControl fullWidth>
                        {/* <InputLabel id="provinsi">Pilih Provinsi</InputLabel> */}
                        <Autocomplete
                          disablePortal
                          id="provinsi"
                          options={provinceData?.data?.map((p) => ({ label: p.name, value: p.id }))}
                          renderInput={(params) => <TextField {...params} placeholder="Provinsi" />}
                          onKeyUp={(e) => console.log('KTP autocomplete', e.target.value)}
                        />

                        {/* <Select
                          labelId="provinsi"
                          id="provinsi"
                          label="Provinsi"
                          error={form?.formState.errors.provinsiId && Boolean(form?.formState?.errors?.provinsiId)}
                          {...field}
                        >
                          {provinceData?.data?.map((p) => (
                            <MenuItem value={p.id} name={p.id} key={p.id}>
                              {p.name}
                            </MenuItem>
                          ))}
                        </Select> */}
                      </FormControl>
                      {form?.formState.errors.provinsiId && <FormHelperText error>Tolong pilih provinsi</FormHelperText>}
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="kabupatenId"
                  control={form.control}
                  defaultValue={1}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Kota
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <FormControl fullWidth>
                        {/* <InputLabel id="provinsi">Pilih Provinsi</InputLabel> */}
                        <Autocomplete
                          disablePortal
                          id="kabupaten"
                          options={regencyData?.data?.map((r) => ({ label: r.name, value: r.id }))}
                          renderInput={(params) => <TextField {...params} placeholder="Kabupaten/Kota" />}
                        />
                        {/* <Select
                          labelId="kabupaten"
                          id="kabupaten"
                          label="Kabupaten/Kota"
                          error={form?.formState.errors.kabupatenId && Boolean(form?.formState?.errors?.kabupatenId)}
                          {...field}
                        >
                          {filteredRegency?.map((r) => (
                            <MenuItem value={r.id} name={r.id} key={r.id}>
                              {r.name}
                            </MenuItem>
                          ))}
                        </Select> */}
                      </FormControl>
                      {form?.formState.errors.kabupatenId && <FormHelperText error>Tolong pilih kota/kabupaten</FormHelperText>}
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="wilayahId"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Kecamatan
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <FormControl fullWidth>
                        <Select
                          labelId="kecamatan"
                          id="kecamatan"
                          label="kecamatan"
                          error={form?.formState.errors.wilayahId && Boolean(form?.formState?.errors?.wilayahId)}
                          {...field}
                        >
                          {wilayahData?.data?.map((w) => (
                            <MenuItem value={w.id} name={w.id} key={w.id}>
                              {w.name}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                      {form?.formState.errors.wilayahId && <FormHelperText error>Tolong pilih kecamatan</FormHelperText>}
                    </Box>
                  )}
                />
              </Grid>
              <Grid item sx={12}>
                <Controller
                  name="agree"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <FormControlLabel
                        control={<Checkbox />}
                        error={form?.formState.errors.agree && Boolean(form?.formState?.errors?.agree)}
                        label="Dengan ini saya menyatakan bahwa data yang saya isikan adalah benar apa adanya dan dapat dipertanggung jawabkan"
                        {...field}
                      />
                      {form?.formState.errors.agree && <FormHelperText error>Centang terlebih dahulu</FormHelperText>}
                    </Box>
                  )}
                />
              </Grid>
            </Grid>
            <Stack direction="row" justifyContent="flex-end" spacing={1.5}>
              <AnimateButton>
                <Button variant="contained" disabled={isDisableStepTwo} onClick={handleNext} sx={{ my: 3, ml: 1, px: 5 }}>
                  Lanjut
                </Button>
              </AnimateButton>
            </Stack>
            {/* </form> */}
          </MainCard>
        </Box>
        <Box sx={{ display: activeStep === 1 ? 'block' : 'none' }}>
          <MainCard title="Upload Foto KTP">
            <KTPImageGuide image="/assets/images/registration/verifikasi-ktp.png" title="Panduan Foto KTP" points={panduanFotoKTP} />
            {/* <KTPImageUpload name="ktp" setKtpImage={setKtp} /> */}

            <Box
              sx={{
                width: '100%',
                border: '1px dashed #263238'
              }}
            >
              <Box
                {...getRootPropsKtp()}
                component="span"
                mt={3}
                py={10}
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                {!selectedFile && (
                  <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <Image src="/assets/images/registration/upload-file.png" alt="Upload" width={100} height={100} />
                    <Typography variant="subtitle1" my={3}>
                      Cari File atau Drag Langsung ke dalam Box ini
                    </Typography>
                  </Box>
                )}
                <Controller
                  name="ktp"
                  control={form.control}
                  render={() => <input type="file" name="ktp" onChange={handleKtpChange} {...getInputPropsKtp()} />}
                />
                {!selectedFile && <Button variant="outlined">Pilih File</Button>}
                {selectedFile && <img src={selectedFile} alt="Thumbnail" style={{ maxWidth: '200px' }} />}
                {selectedFile && (
                  <Stack direction="column" alignItems="center">
                    <Typography align="center" variant="subtitle1" my={1}>
                      {selectedKtpName}
                    </Typography>
                    <Typography align="center" variant="subtitle3">
                      {selectedKtpSize} Kb
                    </Typography>
                  </Stack>
                )}
              </Box>
            </Box>

            <Grid container mt={2}>
              {selectedFile && (
                <Grid item sx={6} sm={3}>
                  <Alert variant="filled" severity="success" sx={{ bgcolor: '#ECFFE8 !important', color: '#557A46' }}>
                    Dokumen Sudah Sesuai
                  </Alert>
                </Grid>
              )}
            </Grid>
            <Stack direction="row" justifyContent="flex-end" spacing={1.5}>
              <AnimateButton>
                {activeStep !== 0 && (
                  <Button onClick={handleBack} variant="outlined" color="error" sx={{ my: 3, ml: 1, px: 5 }}>
                    Kembali
                  </Button>
                )}
              </AnimateButton>
              <AnimateButton>
                <Button variant="contained" disabled={!selectedFile} onClick={handleNext} sx={{ my: 3, ml: 1, px: 5 }}>
                  Lanjut
                </Button>
              </AnimateButton>
            </Stack>
          </MainCard>
        </Box>
        <Box sx={{ display: activeStep === 2 ? 'block' : 'none' }}>
          <MainCard title="Upload Foto Selfie dengan KTP">
            <KTPImageGuide
              image="/assets/images/registration/selfie-ktp.png"
              title="Panduan Selfie dengan KTP"
              points={panduanFotoSelfieKTP}
            />
            <KTPImageUpload name="photo" />
            <Stack direction="row" justifyContent="flex-end" spacing={1.5}>
              <AnimateButton>
                {activeStep !== 0 && (
                  <Button onClick={handleBack} variant="outlined" color="error" sx={{ my: 3, ml: 1, px: 5 }}>
                    Kembali
                  </Button>
                )}
              </AnimateButton>
              <AnimateButton>
                <Button variant="contained" onClick={handleNext} sx={{ my: 3, ml: 1, px: 5 }}>
                  Lanjut
                </Button>
              </AnimateButton>
            </Stack>
          </MainCard>
        </Box>

        <Box sx={{ display: activeStep === 3 ? 'block' : 'none' }}>
          <MainCard title="Upload Buku Rekening">
            <Grid container mb={4} spacing={2}>
              <Grid item xs={12}>
                <Controller
                  name="nama_bank"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Nama Bank
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <TextField
                        fullWidth
                        placeholder="Masukkan nama bank"
                        error={form?.formState.errors.bank && Boolean(form?.formState?.errors?.bank)}
                        {...field}
                      />
                      {form?.formState.errors.bank && <FormHelperText error>Tolong masukkan Bank</FormHelperText>}
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="pemilik_rekening"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Nama Pemilik Rekening
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <TextField
                        fullWidth
                        placeholder="Masukkan nama pemilik rekening"
                        error={form?.formState.errors.pemilik_rekening && Boolean(form?.formState?.errors?.pemilik_rekening)}
                        {...field}
                      />
                      {form?.formState.errors.pemilik_rekening && (
                        <FormHelperText error>Tolong masukkan no. KTP dengan sesuai</FormHelperText>
                      )}
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  name="nomor_rekening"
                  control={form.control}
                  render={({ field }) => (
                    <Box>
                      <InputLabel>
                        Nomor Rekening
                        <Typography component="span" color="red">
                          *
                        </Typography>
                      </InputLabel>
                      <TextField
                        fullWidth
                        placeholder="Masukkan nomor rekening kamu"
                        error={form?.formState.errors.nomor_rekening && Boolean(form?.formState?.errors?.nomor_rekening)}
                        {...field}
                      />
                      {form?.formState.errors.nomor_rekening && <FormHelperText error>Tolong masukkan no. rekenening</FormHelperText>}
                    </Box>
                  )}
                />
              </Grid>
            </Grid>
            <KTPImageUpload name="buku_rekening" />
            <Stack direction="row" justifyContent="flex-end" spacing={1.5}>
              <AnimateButton>
                {activeStep !== 0 && (
                  <Button onClick={handleBack} variant="outlined" color="error" sx={{ my: 3, ml: 1, px: 5 }}>
                    Kembali
                  </Button>
                )}
              </AnimateButton>
              <AnimateButton>
                <Button variant="contained" type="submit" sx={{ my: 3, ml: 1, px: 5 }}>
                  Upload
                </Button>
              </AnimateButton>
            </Stack>
          </MainCard>
        </Box>
      </form>
    </>
  );
};

export default VerifikasiKTPFormWizard;
