import { Alert } from '@mui/material';
import { Box } from '@mui/system';
import Image from 'next/image';

const AlertKTPIcon = () => (
  <Box
    sx={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '26px',
      height: '26px',
      backgroundColor: '#ffc050',
      borderRadius: '50%'
    }}
  >
    <Image src="/assets/images/icons/alert-icon.svg" alt="Alert Icon" width={12} height={12} />
  </Box>
);

const VerifikasiAlert = () => (
  <Alert variant="filled" sx={{ mb: 4, bgcolor: '#fff0d6' }} icon={<AlertKTPIcon />} severity="warning">
    Kamu harus melengkapi data diri kamu dan verifikasi dengan menggunakan KTP
  </Alert>
);

export default VerifikasiAlert;
