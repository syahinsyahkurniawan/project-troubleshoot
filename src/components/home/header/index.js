import PropTypes from 'prop-types';
import { cloneElement, useState } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import {
  AppBar as MuiAppBar,
  Box,
  Button,
  Container,
  Drawer,
  IconButton,
  Link,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Stack,
  Toolbar,
  Typography,
  useScrollTrigger
} from '@mui/material';

// project imports

// assets
import { IconBook, IconCreditCard, IconDashboard, IconHome2 } from '@tabler/icons';
import MenuIcon from '@mui/icons-material/Menu';
import { useRouter } from 'next/router';
import Logo from 'components/ui-component/Logo';
import { useCheckJwt } from 'hooks/utils/useCheckJwt';
import ProfileSection from 'layout/MainLayout/Header/ProfileSection';

function ElevationScroll({ children, window }) {
  const theme = useTheme();
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window
  });

  return cloneElement(children, {
    elevation: trigger ? 1 : 0,
    style: {
      backgroundColor: theme.palette.mode === 'dark' && trigger ? theme.palette.dark[800] : theme.palette.background.default,
      color: theme.palette.text.dark
    }
  });
}

ElevationScroll.propTypes = {
  children: PropTypes.node,
  window: PropTypes.object
};

// ==============================|| MINIMAL LAYOUT APP BAR ||============================== //

const Header = ({ ...others }) => {
  const { jwt } = useCheckJwt();
  const [drawerToggle, setDrawerToggle] = useState(false);

  const drawerToggler = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setDrawerToggle(open);
  };

  const router = useRouter();
  const hash = router.asPath.split('#')[1];

  return (
    <ElevationScroll {...others}>
      <MuiAppBar>
        <Container sx={{ width: '100%' }}>
          <Toolbar sx={{ py: { lg: 2 }, px: `0 !important`, display: 'flex', justifyContent: 'space-between' }}>
            <Typography component="div" sx={{ textAlign: 'left' }}>
              <Logo />
            </Typography>
            <Stack
              direction="row"
              sx={{ display: { xs: 'none', sm: 'flex' }, justifyContent: 'space-between', width: '100%' }}
              spacing={{ xs: 1.5 }}
            >
              <Stack direction="row" justifyContent="space-evenly" px={{ sm: 4, md: 10, lg: 12 }} flexGrow={1}>
                <Box display="inline-block">
                  <Button
                    color="inherit"
                    component={Link}
                    href="#"
                    sx={{ display: 'flex', flexDirection: 'column', fontSize: { sm: 12, lg: 14 }, color: '#2eabff' }}
                  >
                    Beranda
                    <Box
                      sx={{
                        width: '9px',
                        height: '9px',
                        background: 'rgba(46, 171, 255, 1)',
                        borderRadius: '50%',
                        opacity: 1
                      }}
                    ></Box>
                  </Button>
                </Box>
                <Box display="inline-block">
                  <Button
                    color="inherit"
                    component={Link}
                    href="#fitur"
                    sx={{ fontSize: { sm: 12, lg: 14 }, display: 'flex', flexDirection: 'column' }}
                  >
                    Fitur
                    <Box
                      sx={{
                        width: '9px',
                        height: '9px',
                        background: 'rgba(46, 171, 255, 1)',
                        borderRadius: '50%',
                        opacity: hash === '/#fitur' ? 1 : 0
                      }}
                    ></Box>
                  </Button>
                </Box>
                <Box display="inline-block">
                  <Button
                    color="inherit"
                    component={Link}
                    href="/#alur-pengiriman"
                    sx={{ fontSize: { sm: 12, lg: 14 }, display: 'flex', flexDirection: 'column' }}
                  >
                    Layanan
                    <Box
                      sx={{
                        width: '9px',
                        height: '9px',
                        background: 'rgba(46, 171, 255, 1)',
                        borderRadius: '50%',
                        opacity: hash === '/#alur-pengiriman' ? 1 : 0
                      }}
                    ></Box>
                  </Button>
                </Box>
              </Stack>
              {!jwt ? (
                <Stack direction="row" gap={1.5}>
                  <Button
                    component={Link}
                    href="/auth/login"
                    variant="outline"
                    disableElevation
                    sx={{
                      width: { xs: '100%', sm: 'auto' },
                      px: { sm: 2, lg: 5 },
                      py: { sm: 1, lg: 1.25 },
                      backgroundColor: '#ffffff',
                      border: '2px solid #2EABFF',
                      boxSizing: 'border-box !important',
                      color: '#2EABFF '
                    }}
                  >
                    Masuk
                  </Button>
                  <Button
                    component={Link}
                    href="/auth/register"
                    variant="contained"
                    sx={{ width: { xs: '100%', sm: 'auto' }, px: { sm: 2, lg: 5 }, py: { sm: 1, lg: 1.5 }, backgroundColor: '#2EABFF' }}
                  >
                    Daftar
                  </Button>
                </Stack>
              ) : (
                <ProfileSection />
              )}
            </Stack>
            <Box sx={{ display: { xs: 'block', sm: 'none' } }}>
              <IconButton color="inherit" onClick={drawerToggler(true)} size="large">
                <MenuIcon />
              </IconButton>
              <Drawer anchor="top" open={drawerToggle} onClose={drawerToggler(false)}>
                {drawerToggle && (
                  <Box sx={{ width: 'auto' }} role="presentation" onClick={drawerToggler(false)} onKeyDown={drawerToggler(false)}>
                    <List>
                      <Link style={{ textDecoration: 'none' }} href="#" target="_blank">
                        <ListItemButton component="a">
                          <ListItemIcon>
                            <IconHome2 />
                          </ListItemIcon>
                          <ListItemText primary="Beranda" />
                        </ListItemButton>
                      </Link>
                      <Link style={{ textDecoration: 'none' }} href="/login" target="_blank">
                        <ListItemButton component="a">
                          <ListItemIcon>
                            <IconDashboard />
                          </ListItemIcon>
                          <ListItemText primary="Layanan" />
                        </ListItemButton>
                      </Link>
                      <Link style={{ textDecoration: 'none' }} href="https://codedthemes.gitbook.io/berry" target="_blank">
                        <ListItemButton component="a">
                          <ListItemIcon>
                            <IconBook />
                          </ListItemIcon>
                          <ListItemText primary="Fitur" />
                        </ListItemButton>
                      </Link>
                      <Link style={{ textDecoration: 'none' }} href="https://codedthemes.gitbook.io/berry" target="_blank">
                        <ListItemButton component="a">
                          <ListItemIcon>
                            <IconBook />
                          </ListItemIcon>
                          <ListItemText primary="Alur Pengiriman" />
                        </ListItemButton>
                      </Link>
                      <Link style={{ textDecoration: 'none' }} href="https://links.codedthemes.com/hsqll" target="_blank">
                        <ListItemButton component="a">
                          <ListItemIcon>
                            <IconCreditCard />
                          </ListItemIcon>
                          <ListItemText primary="Masuk" />
                        </ListItemButton>
                      </Link>
                      <Link style={{ textDecoration: 'none' }} href="https://links.codedthemes.com/hsqll" target="_blank">
                        <ListItemButton component="a">
                          <ListItemIcon>
                            <IconCreditCard />
                          </ListItemIcon>
                          <ListItemText primary="Daftar" />
                        </ListItemButton>
                      </Link>
                    </List>
                  </Box>
                )}
              </Drawer>
            </Box>
          </Toolbar>
        </Container>
      </MuiAppBar>
    </ElevationScroll>
  );
};

export default Header;
