import styled from '@emotion/styled';
import { Typography } from '@mui/material';
import { Box } from '@mui/system';

const StyledDiv = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignSelf: 'center',
  flexDirection: 'row',
  gap: '20px',
  width: '100%',
  [theme.breakpoints.down('md')]: {
    flexDirection: 'column'
  }
}));
const CustomCard = (props) => {
  const { icon, color, title, content } = props;

  return (
    <StyledDiv>
      <Box sx={{ backgroundColor: color, padding: '24px', borderRadius: '5px', width: '88px', height: '88px' }}>{icon}</Box>
      <Box sx={{ marginTop: '10px', maxWidth: '252px', gap: '10px', display: 'flex', flexDirection: 'column' }}>
        <Typography variant="h3" color="grey.700">
          {title}
        </Typography>
        <Typography variant="body1" color="grey.600">
          {content}
        </Typography>
      </Box>
    </StyledDiv>
  );
};

export default CustomCard;
