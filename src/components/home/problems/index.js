import React from 'react';

import { Grid, Typography } from '@mui/material';

import ImportantDeliveries from 'svgs/ImportantDeliveries';
import DoNotTilt from 'svgs/DoNotTilt';
import RemoveDeliveries from 'svgs/RemoveDeliveries';
import DeliverySettings from 'svgs/DeliverySettings';
import CashOnDelivery from 'svgs/CashOnDelivery';
import { ContentWrapper } from '../container/ContainerContent';
import CustomCard from './CustomCard';

const Problems = () => (
  <ContentWrapper>
    <Typography sx={{ paddingY: '20px' }} fontSize={30} fontWeight={700} color="grey.700">
      Problems
    </Typography>
    <Grid container xs={12} sx={{ gap: '40px', paddingY: '80px', justifyContent: 'center' }}>
      <Grid item>
        <CustomCard
          icon={<ImportantDeliveries />}
          color="primary.light"
          title="Banyak Paket Bermasalah"
          content="Kamu sering mengalami banyak kendala dalam melakukan pengiriman seperti Paket Hilang, Rusak atau Paket dicuri?"
        />
      </Grid>
      <Grid item>
        <CustomCard
          icon={<DoNotTilt />}
          color="error.light"
          title="Ongkir RTS Mahal"
          content="Seringkali Bisnis Owner mengalami masalah dalam membayar Ongkir RTS yang mahal ke Ekspedisi dan Platform"
        />
      </Grid>
      <Grid item>
        <CustomCard
          icon={<RemoveDeliveries />}
          color="orange.light"
          title="ORTS yang tinggi"
          content="RTS yang tinggi disebabkan oleh banyak faktor eksternal, bisa jadi Ekspedisi yang kurang kompeten, Customer yang menolak membeli . dan banyaklagi."
        />
      </Grid>

      <Grid item>
        <CustomCard
          icon={<DeliverySettings />}
          color="#F3DEFF"
          title="Proses manual"
          content="Dalam memanage semua paket pengiriman kamu, kamu masih menggunakan cara yang manual?"
        />
      </Grid>
      <Grid item>
        <CustomCard
          icon={<CashOnDelivery />}
          color="success.light"
          title="Cashback Rendah"
          content="Dalam mengirim volume paket setiap bulannya kamu mendapatkan cashback yang Rendah?"
        />
      </Grid>
    </Grid>
  </ContentWrapper>
);

export default Problems;
