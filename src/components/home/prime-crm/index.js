// material-ui
import { useTheme } from '@mui/material/styles';
import { Button, CardMedia, Grid, Stack, Typography } from '@mui/material';
import { ContentWrapper } from '../container/ContainerContent';
import AnimateButton from 'components/ui-component/extended/AnimateButton';
// project import

const IlustrationImage = '/assets/images/landingpage/dashboard-image.svg';

// ==============================|| LANDING - CUSTOMIZE ||============================== //

const PrimeCRM = () => {
  const theme = useTheme();

  return (
    <ContentWrapper>
      <Grid container justifyContent="space-between" alignItems="center" spacing={{ xs: 3, sm: 5, md: 6, lg: 10 }}>
        <Grid item xs={12} md={6} sx={{ img: { width: '100%' } }}>
          <Stack sx={{ width: '100%', mb: 5, mx: 'auto' }}>
            <CardMedia component="img" image={IlustrationImage} alt="dashboard_image" />
          </Stack>
        </Grid>
        <Grid item xs={12} md={6}>
          <Grid container spacing={2.5}>
            <Grid item xs={12}>
              <Typography variant="h5" sx={{ fontSize: { xs: '1.5rem', sm: '2.125rem' }, mb: 4 }}>
                Jangan Khawatir! Kami Memiliki Pelayanan CRM yang Prima
              </Typography>
              <Typography
                variant="subtitle2"
                color="text.primary"
                sx={{
                  fontSize: '1rem',
                  zIndex: '99',
                  width: { xs: '100%', sm: '100%', md: 'calc(100% - 20%)' }
                }}
              >
                Completed memiliki Pelayanan CRM yang Prima, yang selalu siap dan sigap untuk bantu kamu dalam menghandle dan mengatasi
                semua masalah-masalah pengiriman selama 24 jam non stop.
              </Typography>
              <AnimateButton>
                <Button sx={{ boxShadow: 'none', my: 4, px: 10, py: 2 }} variant="contained" href="#" target="_blank">
                  Gabung Sekarang
                </Button>
              </AnimateButton>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </ContentWrapper>
  );
};

export default PrimeCRM;
