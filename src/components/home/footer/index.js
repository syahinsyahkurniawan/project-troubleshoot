/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
// material-ui
import { styled } from '@mui/material/styles';
import { Box, Container, Grid, Link, Stack, Typography } from '@mui/material';
import { ContentWrapper } from '../container/ContainerContent';

// Link - custom style
const FooterLink = styled(Link)(({ theme }) => ({
  fontSize: '18px',
  color: '#283440',
  '&:hover': {
    color: theme.palette.primary.main
  },
  '&:active': {
    color: theme.palette.primary.main
  }
}));

const Footer = () => (
  <ContentWrapper>
    <Grid container rowGap="40px" alignItems="center" justifyContent="center">
      <Grid item xs={12} md={4}>
        <Grid item xs={12} sm={6} lg={12}>
          <img src="/assets/images/landingpage/header/header-logo.png" style={{ width: '80%', marginTop: '24px' }} />
        </Grid>
        <Stack spacing={{ xs: 3 }} mt={4}>
          <Stack direction="row" spacing={{ xs: 2 }}>
            <Box sx={{ width: '25px', height: '25px', borderRadius: '50%', background: '#d9d9d9' }}></Box>
            <Typography>
              Head Office Completed Rafless Hills <br />
              Rafles Hills Blok AD No 3, Harjamukti, <br />
              Cimanggis, Depok, Jawa Barat 16456
            </Typography>
          </Stack>
          <Stack direction="row" spacing={{ xs: 2 }}>
            <Box sx={{ width: '25px', height: '25px', borderRadius: '50%', background: '#d9d9d9' }}></Box>
            <Typography>admin@completed.id</Typography>
          </Stack>
          <Stack direction="row" spacing={{ xs: 2 }}>
            <Box sx={{ width: '25px', height: '25px', borderRadius: '50%', background: '#d9d9d9' }}></Box>
            <Typography>089671122536</Typography>
          </Stack>
        </Stack>
      </Grid>
      <Grid item xs={12} md={4}>
        <Stack spacing={{ xs: 3, md: 5 }}>
          <Typography variant="h4" color="#283440" sx={{ fontSize: '20px', fontWeight: 500 }}>
            Panduan
          </Typography>
          <Stack spacing={{ xs: 1.5, md: 2.5 }}>
            <FooterLink href="/about-us" color="#283440" target="_blank" underline="none">
              Tentang Kami
            </FooterLink>
            <FooterLink href="/terms-and-condition" target="_blank" underline="none">
              Syarat dan Ketentuan
            </FooterLink>
            <FooterLink href="/privacy-policy" target="_blank" underline="none">
              Kebijakan Privasi
            </FooterLink>
            <FooterLink href="/faq" target="_blank" underline="none">
              FAQ
            </FooterLink>
          </Stack>
        </Stack>
      </Grid>
      <Grid item xs={12} md={4}>
        <Typography variant="h4" color="#283440" sx={{ fontSize: '20px', fontWeight: 500 }}>
          Kerjasama Ekspidisi
        </Typography>
        <Stack mt={4} spacing={{ xs: 1.5, md: 2.5 }} direction="row" flexWrap="wrap" rowGap={2}>
          <img src="/assets/images/landingpage/footer/expedisi/footer-expedisi-ninja-express.png" height={30} />
          <img src="/assets/images/landingpage/footer/expedisi/footer-expedisi-jne.png" height={30} />
          <img src="/assets/images/landingpage/footer/expedisi/footer-expedisi-jnt.png" height={30} />
          <img src="/assets/images/landingpage/footer/expedisi/footer-expedisi-sicepat.png" height={30} />
        </Stack>
        <Typography mt={6} variant="h4" color="#283440" sx={{ fontSize: '20px', fontWeight: 500 }}>
          Follow Kami
        </Typography>
        <Stack mt={4} spacing={{ xs: 1.5, md: 2.5 }} direction="row">
          <Link href="https://www.tiktok.com" target="_blank">
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                width: '52px',
                height: '52px',
                background: 'rgba(0, 104, 174, 1)',
                borderRadius: '50%'
              }}
            >
              <img src="/assets/images/landingpage/footer/social-media/footer-tiktok-icon.png" height={25} />
            </Box>
          </Link>
          <Link href="https://www.instagram.com" target="_blank">
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                width: '52px',
                height: '52px',
                background: 'rgba(0, 104, 174, 1)',
                borderRadius: '50%'
              }}
            >
              <img src="/assets/images/landingpage/footer/social-media/footer-ig-icon.png" height={25} />
            </Box>
          </Link>
          <Link href="https://www.youtube.com" target="_blank">
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                width: '52px',
                height: '52px',
                background: 'rgba(0, 104, 174, 1)',
                borderRadius: '50%'
              }}
            >
              <img src="/assets/images/landingpage/footer/social-media/footer-yt-icon.png" height={25} />
            </Box>
          </Link>
          <Link href="https://www.twitter.com" target="_blank">
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                width: '52px',
                height: '52px',
                background: 'rgba(0, 104, 174, 1)',
                borderRadius: '50%'
              }}
            >
              <img src="/assets/images/landingpage/footer/social-media/footer-twitter-icon.png" height={25} />
            </Box>
          </Link>
        </Stack>
      </Grid>
    </Grid>

    <Box sx={{ bgcolor: 'dark.dark', py: { xs: 3, sm: 1.5 } }}>
      <Stack direction={{ xs: 'column', sm: 'row' }} alignItems="center" justifyContent="center" spacing={{ xs: 1.5, sm: 1, md: 3 }}>
        <Typography align="center" color="text.secondary">
          Copyright 2023 @ Completed App. All rights reserved
        </Typography>
      </Stack>
    </Box>
  </ContentWrapper>
);
export default Footer;
