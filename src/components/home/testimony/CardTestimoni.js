/* eslint-disable @next/next/no-img-element */
import styled from '@emotion/styled';
import { Box } from '@mui/system';
import Image from 'next/image';
import PropTypes from 'prop-types';

const CardTestimoniWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 364px;
  height: 280px;
  justify-content: space-between;
  padding: 32px 24px;
  margin: 64px 0 32px 0;
  border-radius: 8px;
  box-shadow: 0px 4px 25px 0px rgba(188, 227, 253, 0.3);
  @media (max-width: 576px) {
  }
`;

const CardTestimoniQuote = styled.div`
  position: absolute;
  top: -48px;
  left: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 66px;
  height: 66px;
  background: rgba(46, 171, 255, 1);
  border-radius: 50%;
`;

const CardUserComment = styled.span`
  font-size: 14px;
  line-height: 25px;
  font-weight: 500;
  color: rgba(77, 81, 84, 1);
`;

const CardUserProfile = styled.div`
  display: flex;
  gap: 16px;
`;

const CardUserProfilePic = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  width: 60px;
  height: 60px;
  border-radius: 50%;
`;

const CardUserProfileName = styled.span`
  font-size: 15px;
  font-weight: 500;
  line-height: 30px;
`;

const CardUserProfileOccupation = styled.span`
  font-size: 14px;
  line-height: 25px;
`;

const CardTestimoni = ({ data }) => (
  <CardTestimoniWrapper>
    <CardTestimoniQuote>
      <Image src="/assets/images/landingpage/testimoni/quotes-vector.png" alt="Quotes Icon Vector" width={24} height={17} />
    </CardTestimoniQuote>
    <CardUserComment>{data.testimonial}</CardUserComment>
    <CardUserProfile>
      <Box>
        <CardUserProfilePic>
          <img src={data.photo} alt={`${data.name} profile pic`} />
        </CardUserProfilePic>
      </Box>
      <Box display="flex" flexDirection="column">
        <CardUserProfileName>{data.name}</CardUserProfileName>
        <CardUserProfileOccupation>{data.company}</CardUserProfileOccupation>
      </Box>
    </CardUserProfile>
  </CardTestimoniWrapper>
);

CardTestimoni.propTypes = {
  data: PropTypes.object
};

export default CardTestimoni;
