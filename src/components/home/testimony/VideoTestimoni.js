import { ThemeProvider, Typography, createTheme } from '@mui/material';

const theme = createTheme({
  typography: {
    sectionTitleVariant: {
      fontSize: 30,
      lineHeight: 1.5,
      fontWeight: 700
    }
  }
});

const VideoTestimoni = () => (
  <ThemeProvider theme={theme}>
    <Typography align="center" component="h2" variant="sectionTitleVariant">
      Apa Kata{' '}
      <Typography component="span" color="#2EABFF" variant="sectionTitleVariant">
        Mereka
      </Typography>
    </Typography>
    <div style={{ display: 'flex', justifyContent: 'center', marginTop: 48 }}>
      <img src="/assets/images/landingpage/testimoni/video-testimoni.png" style={{ width: '85%', height: 'auto' }} alt="video" />
    </div>
  </ThemeProvider>
);

export default VideoTestimoni;
