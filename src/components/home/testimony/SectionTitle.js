import { ThemeProvider, Typography, createTheme } from '@mui/material';

const theme = createTheme({
  typography: {
    sectionTitleVariant: {
      fontSize: 30,
      lineHeight: 1.5,
      fontWeight: 700
    }
  }
});

const SectionTitle = () => (
  <ThemeProvider theme={theme}>
    <Typography align="center" component="h2" variant="sectionTitleVariant">
      <Typography component="span" color="#2EABFF" variant="sectionTitleVariant">
        Completed
      </Typography>{' '}
      Telah Dipercaya
      <Typography component="span" display="block" variant="sectionTitleVariant">
        oleh Pebisnis dan UMKM di Indonesia
      </Typography>
    </Typography>
  </ThemeProvider>
);

export default SectionTitle;
