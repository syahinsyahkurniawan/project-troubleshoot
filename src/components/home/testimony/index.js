import styled from '@emotion/styled';
import SectionTitle from './SectionTitle';
import VideoTestimoni from './VideoTestimoni';
import CardTestimoni from './CardTestimoni';
import testimonyData from '../../../data/dummy/testimoni/index.json';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import Slider from 'react-slick';
import { Box } from '@mui/system';
import { useLanding } from 'hooks/queries/master-data/useLanding';

const SectionTestimoniWrapper = styled.div`
  padding: 120px 60px;
  @media (max-width: 576px) {
    padding: 120px 20px;
  }
`;

const Testimony = () => {
  const { data } = useLanding();
  const settings = {
    dots: true,
    arrows: false,
    autopaly: true,
    className: 'center',
    infinite: true,
    centerPadding: '60px',
    slidesToShow: 2,
    slidesToScroll: 3,
    speed: 500,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 1534,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          dots: true
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true
        }
      }
    ],
    customPaging: (i) => <CustomDot />
  };
  return (
    <SectionTestimoniWrapper>
      <SectionTitle />
      <Box mt={4} mb={5}>
        <Slider {...settings}>
          {data?.data?.testimonial?.map((t) => (
            <CardTestimoni data={t} />
          ))}
        </Slider>
      </Box>
      <VideoTestimoni />
    </SectionTestimoniWrapper>
  );
};

export default Testimony;

const CustomDot = ({ onClick, active }) => (
  <Box
    className="custom-dot"
    sx={{ width: 20, height: 20, background: active ? 'rgba(0, 104, 174, 1)' : 'rgb(241, 241, 241)', borderRadius: '50%' }}
    onClick={onClick}
  ></Box>
);
