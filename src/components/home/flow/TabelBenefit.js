import styled from '@emotion/styled';
import CardTableBenefit from './CardTableBenefit';
import benefitData from '../../../data/dummy/benefit/index.json';
import { useLanding } from 'hooks/queries/master-data/useLanding';

const TableBenefitWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 120px;
  @media (max-width: 576px) {
    overflow-x: scroll;
  }
`;

const Table = styled.div`
  border-collapse: separate;
  border-spacing: 0;
  display: none;
  @media (min-width: 577px) {
    display: block;
  }
  @media (max-width: 1024px) {
    transform: scale(0.7);
  }
`;

const TableBenefitRow = styled.div`
  display: flex;
  margin-bottom: 16px;
  height: 110px;
  &:nth-child(1) {
    height: 80px;
    background-color: rgba(46, 171, 255, 1);
  }
  @media (max-width: 576px) {
    height: 88px;
    &:nth-child(1) {
      height: 64px;
      background-color: rgba(46, 171, 255, 1);
    }
  }
`;

const TableBenefitHeading = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 210px;
  margin-right: 16px;
  background-color: rgba(0, 104, 174, 1);
  color: #ffffff;
  font-size: 20px;
  font-weight: 600;
  line-height: 30px;
  text-align: center;
  border-radius: 4px;
  @media (max-width: 576px) {
    width: 70px;
    font-size: 14px;
    line-height: 20px;
  }
`;

const TableBenefitSubHeading = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 240px;
  text-align: center;
  background-color: rgba(46, 171, 255, 1);
  font-size: 20px;
  font-weight: 600;
  color: #ffffff;
  @media (max-width: 576px) {
    width: 80px;
    font-size: 12px;
  }
`;

const TableBenefitItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 240px;
  padding: 24px;
  margin-top 16px;
  text-align: center;
  font-size: 18px;
  line-height: 30px;
  background-color: rgba(248, 248, 248, 1);
  @media (max-width: 576px) {
    width: 80px;
    font-size: 12px;
    line-height: 16px;
  }
`;

const TableMobile = styled.div`
  display: none;
  @media (max-width: 576px) {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    gap: 16px;
  }
`;

const TableBenefit = () => {
  const { data } = useLanding();
  return (
    <TableBenefitWrapper>
      <Table>
        <TableBenefitRow>
          <TableBenefitHeading>Benefit</TableBenefitHeading>
          {data?.data?.ekspedisi.map((data, index) => (
            <TableBenefitSubHeading id={index}>{data.name}</TableBenefitSubHeading>
          ))}
        </TableBenefitRow>
        <TableBenefitRow>
          <TableBenefitHeading>Biaya COD</TableBenefitHeading>
          {data?.data?.ekspedisi.map((data, index) => (
            <TableBenefitItem id={index}>{data.cost_cod}</TableBenefitItem>
          ))}
        </TableBenefitRow>
        <TableBenefitRow>
          <TableBenefitHeading>Biaya Retur</TableBenefitHeading>
          {data?.data?.ekspedisi.map((data, index) => (
            <TableBenefitItem id={index}>{data.cost_retur}</TableBenefitItem>
          ))}
        </TableBenefitRow>
        <TableBenefitRow>
          <TableBenefitHeading>Cashback</TableBenefitHeading>
          {data?.data?.ekspedisi.map((data, index) => (
            <TableBenefitItem id={index}>{data.cost_retur}</TableBenefitItem>
          ))}
        </TableBenefitRow>
        <TableBenefitRow>
          <TableBenefitHeading>
            Support <br /> Operasional
          </TableBenefitHeading>
          {data?.data?.ekspedisi.map((data, index) => (
            <TableBenefitItem id={index}>{data.operational_support}</TableBenefitItem>
          ))}
        </TableBenefitRow>
      </Table>

      <TableMobile>
        {benefitData.map((b) => (
          <CardTableBenefit
            name={b.name}
            icon={b.icon}
            codPrice={b.codPrice}
            cashback={b.cashback}
            retur={b.retur}
            supportOperational={b.supportOperational}
          />
        ))}
      </TableMobile>
    </TableBenefitWrapper>
  );
};

export default TableBenefit;
