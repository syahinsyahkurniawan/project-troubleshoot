import SectionTitle from './SectionTitle';
import AlurPengiriman from './AlurPengiriman';
import styled from '@emotion/styled';
import TableBenefit from './TabelBenefit';

const SectionAlurWrapper = styled.div`
  padding: 120px 0;
  @media (max-width: 768px) {
    transform: scale(0.8);
    padding: 20px 0;
  }
`;

const Flow = () => (
  <SectionAlurWrapper id="alur-pengiriman">
    <SectionTitle />
    <AlurPengiriman />
    <TableBenefit />
  </SectionAlurWrapper>
);

export default Flow;
