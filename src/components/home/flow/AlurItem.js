import PropTypes from 'prop-types';
import AlurText from './AlurText';
import AlurIcon from './AlurIcon';
import styled from '@emotion/styled';

const AlurItemWrapper = styled.div`
  display: flex;
  margin-top: ${(props) => (props.isEven ? '96px' : 0)};
  flex-direction: column;
  width: 130px;
  @media (max-width: 1024px) {
    width: 100px;
  }
  @media (max-width: 576px) {
    margin: 0 0 24px 0;
  }
`;

const AlurItem = ({ num = '00', color = '#0881A3', text = 'Teks Langkah Alur', icon = '/assets/images/landingpage/alur/alur-01.png' }) => (
  <AlurItemWrapper isEven={num % 2 === 0} color="blue">
    <AlurIcon num={num} color={color} text={text} icon={icon} />
    <AlurText text={text} />
  </AlurItemWrapper>
);

AlurItem.propTypes = {
  num: PropTypes.string,
  color: PropTypes.string,
  text: PropTypes.string,
  icon: PropTypes.string
};

export default AlurItem;
