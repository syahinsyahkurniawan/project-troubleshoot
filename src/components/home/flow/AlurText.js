import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const AlurTextTypography = styled.span`
  font-size: 25px;
  font-weight: 500;
  line-height: 1.28;
  margin-top: 48px;
  color: #494848;
  @media (max-width: 576px) {
    margin-top: 24px;
    width: 16px;
  }
`;

const AlurText = ({ text }) => <AlurTextTypography>{text}</AlurTextTypography>;

AlurText.propTypes = {
  text: PropTypes.string
};

export default AlurText;
