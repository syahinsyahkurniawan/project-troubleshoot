import styled from '@emotion/styled';
import { Box } from '@mui/system';
import PropTypes from 'prop-types';

const CardTableBenefitWrapper = styled.div`
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 352px;
  height: 325px;
  padding: 0 24px;
  border-radius: 16px;
  box-shadow: 0px 2px 25px 0px rgba(25, 91, 106, 0.4);
`;

const CardIconWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 105px;
  height: 105px;
  @media (max-width: 576px) {
    width: 90%;
  }
`;

const CardHeading = styled.p`
  font-size: 22px;
  color: rgba(40, 52, 64, 1);
  text-align: center;
  margin-top: 0;
`;

const CardText = styled.p`
  font-size: 14px;
  color: rgba(80, 80, 80, 1);
  text-align: center;
  margin-top: 0;
`;

const CardTableBenefit = ({ name, icon, codPrice, cashback, retur, supportOperational }) => (
  <CardTableBenefitWrapper>
    <CardIconWrapper>
      <img src={icon} alt={name} />
    </CardIconWrapper>
    <CardHeading>{name}</CardHeading>
    <CardText>
      <strong>Biaya COD</strong>
      <Box>{codPrice}</Box>
    </CardText>
    <CardText>
      <strong>Biaya Retur</strong>
      <Box>{retur}</Box>
    </CardText>
    <CardText>
      <strong>Cashback</strong>
      <Box>{cashback}</Box>
    </CardText>
    <CardText>
      <strong>Support Operasional</strong>
      <Box>{supportOperational}</Box>
    </CardText>
  </CardTableBenefitWrapper>
);

CardTableBenefit.propTypes = {
  name: PropTypes.string,
  icon: PropTypes.string,
  codPrice: PropTypes.string,
  cashback: PropTypes.string,
  retur: PropTypes.string,
  supportOperational: PropTypes.string
};

export default CardTableBenefit;
