import { alurPengiriman } from 'data/dummy/alur-pengiriman';
import AlurItem from './AlurItem';
import styled from '@emotion/styled';
import { addZeroes } from 'utils/commons/addZeroes';
import { ContentWrapper } from '../container/ContainerContent';

const AlurPengirimanWrapper = styled.div`
  display: flex;
  justify-content: center;
  gap: 72px;

  @media (max-width: 576px) {
    flex-direction: column;
    align-items: center;
    gap: 16px;
  }
`;

const AlurPengiriman = () => (
  <AlurPengirimanWrapper>
    {alurPengiriman.map((alurItem, i) => (
      <AlurItem num={addZeroes(i + 1)} text={alurItem.text} color={alurItem.colorTheme} icon={alurItem.icon} />
    ))}
  </AlurPengirimanWrapper>
);

export default AlurPengiriman;
