/* eslint-disable jsx-a11y/alt-text */
import styled from '@emotion/styled';
import { Typography } from '@mui/material';
import { Box } from '@mui/system';
import fiturData from '../../../data/dummy/fitur/index.json';
import CardFitur from './CardFitur';
import { useLanding } from 'hooks/queries/master-data/useLanding';
import Image from 'next/image';

const SectionFiturWrapper = styled.div``;

const Feature = () => {
  const { data } = useLanding();
  return (
    <SectionFiturWrapper id="fitur">
      <Box
        flexDirection={{ xs: 'column', sm: 'row' }}
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          gap: 32,
          height: 200,
          width: '100%',
          borderRadius: '75%',
          backgroundColor: 'rgba(245, 251, 255, 1)'
        }}
      >
        <img src="/assets/images/landingpage/fitur/fitur-logo.png" width={180} height={44} />
        <Typography fontSize={{ sm: 14, lg: 22 }} textAlign="center" lineHeight={{ sm: '20px', lg: '32px' }}>
          hadir sebagai solusi untuk memudahkan pengiriman kamu <br /> dengan berbagai benefit tanpa minimum jumlah pengiriman
        </Typography>
      </Box>

      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          flexWrap: 'wrap',
          columnGap: 32,
          marginTop: '-96px',
          rowGap: 64,
          padding: '172px 0 300px',
          backgroundColor: 'rgba(245, 251, 255, 1)',
          backgroundSize: 'cover',
          backgroundImage: 'url(assets/images/landingpage/fitur/fitur-bg.png)',
          backgroundPosition: '40% 0'
        }}
      >
        {fiturData.map((f) => (
          <CardFitur icon={f.icon} title={f.title} description={f.description} />
        ))}
      </div>
      <Box
        sx={{
          background: '#ffffff',
          display: 'flex',
          flexDirection: 'column',
          margin: 'auto',
          justifyContent: 'center',
          alignItems: 'center',
          width: 'fit-content',
          padding: '12px 32px 32px',
          marginTop: '-148px',
          borderRadius: '148px',
          boxShadow: '0px 6px 25px 0px rgba(188, 227, 253, 0.25)'
        }}
      >
        <Typography
          fontSize={{ sm: '12px', md: '14px', lg: '18px' }}
          lineHeight={{ sm: '16px', md: '20px', lg: '28px' }}
          fontWeight={600}
          textAlign="center"
          marginTop={0}
        >
          Kami telah bekerjasama dengan Ekspedisi ternama <br />
          yang sudah didukung dengan pengiriman COD dan Non-COD
        </Typography>
        {data?.data?.ekspedisi.map((data, index) => (
          <Image id={index} src={data.logo} width={135} height={50} alt={data.name} />
        ))}
        {/* <img src="/assets/images/landingpage/fitur/fitur-rekan.png" style={{ width: '80%', height: 'auto' }} /> */}
      </Box>
    </SectionFiturWrapper>
  );
};

export default Feature;
