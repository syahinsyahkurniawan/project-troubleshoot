/* eslint-disable @next/next/no-img-element */
import styled from '@emotion/styled';
import PropTypes from 'prop-types';

const CardFiturWrapper = styled.div`
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 352px;
  height: 325px;
  padding: 0 48px;
  border-radius: 16px;
  box-shadow: 0px 2px 25px 0px rgba(25, 91, 106, 0.4);
  // z-index: 2;
`;

const CardIconWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 105px;
  height: 105px;
  background: rgba(241, 252, 255, 1);
  border-radius: 100%;
`;

const CardHeading = styled.h2`
  font-size: 22px;
  fontweight: 600;
  color: rgba(40, 52, 64, 1);
`;

const CardDescription = styled.p`
  font-size: 14px;
  height: 100px;
  color: rgba(80, 80, 80, 1);
  text-align: center;
`;

const CardFitur = ({ title, description, icon }) => (
  <CardFiturWrapper>
    <CardIconWrapper>
      <img src={icon} alt={title} width={54} height={54} />
    </CardIconWrapper>
    <CardHeading>{title}</CardHeading>
    <CardDescription>{description}</CardDescription>
  </CardFiturWrapper>
);

CardFitur.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  icon: PropTypes.string
};

export default CardFitur;
