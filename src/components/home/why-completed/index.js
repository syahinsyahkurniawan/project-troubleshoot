// material-ui
import { useTheme } from '@mui/material/styles';
import { Card, CardMedia, Grid, Stack, Typography } from '@mui/material';
import { useState } from 'react';
import { ContentWrapper } from '../container/ContainerContent';

const RubyIcon = 'assets/images/landingpage/why-completed/ruby-gemstone.svg';
const InsIdea = 'assets/images/landingpage/why-completed/insidea.svg';
const Delivery = 'assets/images/landingpage/why-completed/delivery.svg';
const Sell = 'assets/images/landingpage/why-completed/sell.svg';
const Image1 = 'assets/images/landingpage/why-completed/image1.svg';
const Image2 = 'assets/images/landingpage/why-completed/image2.svg';
const Image3 = 'assets/images/landingpage/why-completed/image3.svg';
const Image4 = 'assets/images/landingpage/why-completed/image4.svg';
const data = [
  {
    index: 1,
    title: 'Pencairan Setiap Hari',
    description:
      'Completed memberikan kenyamanan berbisnis dengan Pencairan Cashback ongkir yang fleksibel setiap hari kapanpun kamu request',
    image: Image1,
    icon: RubyIcon
  },
  {
    index: 2,
    title: 'Support Pelatihan dan Pengembangan',
    description:
      'Completed ga cuman support kamu di manage pengiriman kamu. tapi juga support kamu untuk mengembangkan bisnis kamu dengan E-course dan juga Workshop',
    image: Image2,
    icon: InsIdea
  },
  {
    index: 3,
    title: 'Kelola Order Serba Praktis',
    description: 'Menggunakan Teknologi paling update untuk mengelola dashboard penjualan kamu',
    image: Image3,
    icon: Delivery
  },
  {
    index: 4,
    title: 'Pelayanan 24 Jam',
    description: 'Menggunakan Teknologi paling update untuk mengelola dashboard penjualan kamu',
    image: Image4,
    icon: Sell
  }
];
const WhyUs = () => {
  const theme = useTheme();
  const [activeItem, setActiveItem] = useState({
    index: 1,
    title: 'Pencairan Setiap Hari',
    description:
      'Completed memberikan kenyamanan berbisnis dengan Pencairan Cashback ongkir yang fleksibel setiap hari kapanpun kamu request',
    image: Image1,
    icon: RubyIcon
  });
  const listSX = {
    display: 'flex',
    alignItems: 'center',
    gap: '0.7rem',
    padding: '10px 0',
    fontSize: '1rem',
    color: theme.palette.grey[900],
    svg: { color: theme.palette.secondary.main }
  };

  return (
    <ContentWrapper>
      <Grid item container xs={12}>
        <Stack sx={{ display: 'flex', flexDirection: 'row', paddingLeft: '3vw', marginTop: '9vw' }}>
          <Typography variant="h2" sx={{ fontSize: { xs: '1.5rem', sm: '2.125rem' }, mb: 0, mr: 1 }}>
            Kenapa
          </Typography>
          <Typography variant="h2" sx={{ fontSize: { xs: '1.5rem', sm: '2.125rem' }, mb: 4, color: '#2EABFF' }}>
            Completed
          </Typography>
        </Stack>
        <Grid container justifyContent="space-between" spacing={{ xs: 3, sm: 5, md: 6, lg: 10 }}>
          <Grid item xs={12} md={6} sx={{ marginBottom: '10px' }}>
            <Grid container xs={12} spacing={0.5} sx={{ display: 'flex', justifyContent: 'space-around' }}>
              {data.map((data) => (
                <Grid item key={data.index}>
                  <Card
                    onMouseEnter={() => setActiveItem(data)}
                    sx={{ width: { xs: '70px', md: '19vw' }, height: { xs: '70px', md: '21vw' }, maxWidth: '212px', maxHeight: '222px' }}
                  >
                    <Stack
                      sx={{
                        height: '100%',
                        width: '100%',
                        backgroundColor: activeItem.index === data.index ? '#2EABFF' : '#FFFFFF',
                        borderRadius: '10px',
                        transition: 'background-color 0.3s, color 0.3s'
                      }}
                    >
                      <Stack
                        sx={{
                          transition: 'background-color 0.3s, color 0.3s',
                          backgroundColor: activeItem.index === data.index ? '#F1FCFF' : '#FFFFFF',
                          borderRadius: '50%',
                          height: { xs: '67px', md: '4vw' },
                          width: { xs: '67px', md: '4vw' },
                          minWidth: { xs: '67px', md: '47px' },
                          minHeight: { xs: '67px', md: '47px' },
                          mx: 'auto',
                          diplay: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                          flexDirection: 'column',
                          marginTop: { xs: '2px', md: '60px' }
                        }}
                      >
                        <CardMedia
                          sx={{ width: { xs: '30px', md: '3vw' }, height: { xs: '30px', md: '3vw' } }}
                          component="img"
                          image={data.icon}
                          alt="Layer"
                        />
                      </Stack>
                      <Typography
                        sx={{
                          diplay: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                          flexDirection: 'column',
                          paddingTop: '25px',
                          paddingBottom: '50px',
                          color: activeItem.index === data.index ? '#FFF' : '#000',
                          fontSize: '1em',
                          fontWeight: 600,
                          textAlign: 'center',
                          display: { xs: 'none', md: 'block' }
                        }}
                      >
                        {data.title}
                      </Typography>
                    </Stack>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Grid>

          <Grid
            item
            xs={12}
            md={6}
            sx={{
              position: 'relative',
              alignItems: 'center'
            }}
          >
            <Stack
              sx={{
                backgroundColor: '#F1FCFF',
                borderRadius: '50%',
                height: '67px',
                width: '67px',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: '55px',
                marginBottom: '85px',
                marginLeft: '69px'
              }}
            >
              <CardMedia sx={{ width: '30px', height: '30px' }} component="img" image={activeItem.icon} alt="Layer" />
            </Stack>
            <Typography sx={{ fontSize: '100px', fontWeight: '500', color: '#5C748B' }}>#{activeItem.index}</Typography>
            <Typography sx={{ fontSize: '20px', fontWeight: '600' }}>{activeItem.title}</Typography>
            <Typography sx={{ fontSize: '18px', fontWeight: '500', paddingTop: '22px' }}>{activeItem.description}</Typography>
            <Stack sx={{ position: 'absolute', top: 0, right: 0, height: '426px', width: '322px' }}>
              <CardMedia component="img" image={activeItem.image} alt="Layer" />
            </Stack>
          </Grid>
        </Grid>
      </Grid>
    </ContentWrapper>
  );
};

export default WhyUs;
