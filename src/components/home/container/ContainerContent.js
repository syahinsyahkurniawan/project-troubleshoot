import styled from '@emotion/styled';

export const ContentWrapper = styled('div')(({ theme }) => ({
  width: '100%',
  maxWidth: '1400px',
  margin: '0 auto',
  display: 'flex',
  position: 'relative',
  flexDirection: 'column',
  alignItems: 'center',
  gap: '40px',
  [theme.breakpoints.down('xl')]: {
    maxWidth: '1180px'
  },
  [theme.breakpoints.down('md')]: {
    padding: '0 25px'
  }
}));
