import { ThemeProvider, Typography, createTheme } from '@mui/material';

const theme = createTheme({
  typography: {
    sectionTitleVariant: {
      fontSize: 30,
      lineHeight: 1.5,
      fontWeight: 700
    }
  }
});
