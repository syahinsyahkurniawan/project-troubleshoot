import { Button, Grid, ThemeProvider, Typography, createTheme } from '@mui/material';
import { ContentWrapper } from '../container/ContainerContent';
import { Box } from '@mui/system';

const theme = createTheme({
  typography: {
    sectionTitleVariant: {
      fontSize: 30,
      lineHeight: 1.5,
      fontWeight: 700
    },
    sectionDescVariant: {
      fontSize: 18,
      lineHeight: '30px'
    },
    sectionHeadingVariant: {
      fontSize: 60,
      fontWeight: 700,
      lineHeight: '45px',
      color: 'rgba(88, 188, 255, 1)'
    }
  }
});

const Hero = () => (
  <ContentWrapper>
    <ThemeProvider theme={theme}>
      <Grid container justifyContent="center" alignItems="center" sx={{ flexDirection: { xs: 'column-reverse', sm: 'row' } }}>
        <Grid item xs={12} sm={6}>
          <Typography component="h2" variant="sectionTitleVariant" marginBottom={2}>
            Platform Pengiriman
            <Typography component="span" color="#2EABFF" variant="sectionTitleVariant">
              {' '}
              No.1{' '}
            </Typography>
            untuk Kirim Barang, Mudah dan Terjangkau
          </Typography>
          <Typography variant="sectionDescVariant">
            Dapatkan diskon ongkir <strong>mulai dari 25%</strong> dan <strong>Cashback 50%!!</strong> Pelajari lebih lanjut untuk tawaran
            menarik lainnya
          </Typography>
          <Grid container mt={6} justifyContent={{ xs: 'center', sm: 'flex-start' }}>
            <Box sx={{ display: 'flex', flexDirection: 'row' }}>
              <Typography variant="sectionHeadingVariant" textAlign={{ xs: 'center', sm: 'left' }}>
                200+
              </Typography>

              <div style={{ display: 'flex', flexDirection: 'row', gap: '4px', alignItems: 'center' }}>
                <Typography variant="sectionDescVariant">Pebisnis online sudah bergabung dengan</Typography>
                <Typography variant="sectionDescVariant" fontWeight={700} component="span" color="primary.main">
                  Completed
                </Typography>
              </div>
            </Box>
          </Grid>
          <Button
            variant="contained"
            sx={{ width: { xs: '100%', sm: 'auto' }, px: { sm: 2, lg: 5 }, py: { sm: 1, lg: 2 }, mt: 3, backgroundColor: '#2EABFF' }}
          >
            Gabung Sekarang
          </Button>
        </Grid>
        <Grid item xs={12} sm={6} mt={{ xs: 4, md: 0 }}>
          <img
            src="/assets/images/landingpage/hero/hero.png"
            alt="Hero Banner"
            width={0}
            height={0}
            style={{ width: '100%', height: 'auto' }}
          />
        </Grid>
      </Grid>
    </ThemeProvider>
  </ContentWrapper>
);

export default Hero;
