// material-ui
import { styled, useTheme } from '@mui/material/styles';
import { Container, Grid, Typography, Stack } from '@mui/material';

// project imports
import LAYOUT from 'constant';
import Layout from 'layout';

import MainCard from 'components/ui-component/cards/MainCard';
import FaqAccordion from 'components/ui-component/extended/FaqAccordion';
import { gridSpacing } from 'store/constant';
import { useLanding } from 'hooks/queries/master-data/useLanding';

import { ContentWrapper } from '../container/ContainerContent';

const HeaderWrapper = styled('div')(({ theme }) => ({
  backgroundSize: '100% 600px',
  backgroundAttachment: 'fixed',
  backgroundRepeat: 'no-repeat',
  textAlign: 'center',
  paddingTop: 30,
  [theme.breakpoints.down('md')]: {
    paddingTop: 0
  }
}));

const faqData = [
  {
    id: '1',
    name: 'Apa itu Completed?',
    defaultExpand: true,
    description:
      'Completed adalah platform Pengiriman  yang bekerjasama dengan Ekspedisi danPergudangan, serta platform dengan fitur yang lengkap untuk menunjang kebutuhan pemilik usaha dalam mengembangkan Bisnis.'
  },
  {
    id: '2',
    name: 'Bagaimana cara daftar completed?',
    description: 'Cara daftarnya bisa mealalui website ini atau melalui customer service kami.'
  },
  {
    id: '3',
    name: 'Apakah ada biaya retur di completed? jika ada, berapa biayanya?',
    description: 'biayanya sangatlah murah dan terjangkau tergantung dari pilihan ekspedisinya.'
  },
  {
    id: '4',
    name: 'Apakah ada diskon ongkir di Completed?',
    description: 'Tentu saja ada dan kami juga memiliki sistem cashback.'
  },
  {
    id: '5',
    name: 'Apakah ada biaya administrasi tiap bulannya?',
    description: 'Biaya administrasi ada, namun sangatlah kecil dan terjangkau.'
  }
];

// ============================|| SAAS PAGES - FAQs ||============================ //

const Faqs = () => {
  const theme = useTheme();
  const { data } = useLanding();
  return (
    <ContentWrapper>
      <Container>
        <Grid item xs={12}>
          <Typography
            variant="h1"
            color="#283440"
            component="div"
            sx={{
              fontSize: '30px',
              fontWeight: 700,
              lineHeight: 1.4,
              paddingBottom: '42px',
              [theme.breakpoints.down('md')]: { fontSize: '1.8125rem', marginTop: '80px' }
            }}
          >
            FAQ
          </Typography>
        </Grid>
        <Grid container justifyContent="center" spacing={gridSpacing}>
          <Grid item xs={12}>
            <MainCard sx={{ textAlign: 'left' }} border={false}>
              {data?.data?.faq.map((data, index) => (
                <Stack sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', fontFamily: 'inter' }}>
                  <Typography
                    sx={{
                      fontSize: '55px',
                      fontWeight: 600,
                      paddingRight: '100px',
                      width: '75px',
                      fontFamily: 'inter',
                      margin: 'auto',
                      textAlign: 'center'
                    }}
                  >
                    {(index + 1).toString().padStart(2, '0')}
                  </Typography>
                  <FaqAccordion data={data} />
                </Stack>
              ))}
            </MainCard>
          </Grid>
        </Grid>
        <Stack to="/" variant="a" component="div">
          <Typography
            sx={{
              fontFamily: 'inter',
              fontSize: '18px',
              fontWeight: 'bold',
              textAlign: 'right',
              paddingRight: '65px',
              marginTop: '30px',
              cursor: 'pointer',
              textDecoration: 'underline',
              color: 'blue'
            }}
          >
            Lihat FAQ Lainnya
          </Typography>
        </Stack>
      </Container>
    </ContentWrapper>
  );
};

Faqs.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.minimal}>{page}</Layout>;
};

export default Faqs;
