import * as React from 'react';
import { useState } from 'react';
import { useTheme } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea, Grid } from '@mui/material';
import { ContentWrapper } from 'components/home/container/ContainerContent.js';
import ProgressCard from './ProgressCard';
import ProfileVerificationProgress from 'svgs/ProfileVerificationProgress';
import ProfileVerified from 'svgs/ProfileVerified';
import AddOrder from 'svgs/AddOrder';
import { Stack } from '@mui/system';

const data = [
  {
    index: 1,
    title: 'Lengkapi profil',
    icon: <ProfileVerificationProgress />
  },
  {
    index: 2,
    title: 'Verifikasi',
    icon: <ProfileVerified />
  },
  {
    index: 3,
    title: 'Tambah order',
    icon: <AddOrder />
  }
];

const VerificationProgress = () => {
  const theme = useTheme();
  const [activeItem, setActiveItem] = useState({
    index: 1,
    title: 'Lengkapi profil',
    icon: ProfileVerificationProgress
  });
  return (
    <Grid item xs={12} md={12} sx={{ marginBottom: '10px' }}>
      <Grid container xs={12} spacing={0.5} sx={{ display: 'flex-start', gap: '14px' }}>
        {data.map((data) => (
          <Grid item key={data.index}>
            <Card onMouseEnter={() => setActiveItem(data)} sx={{ width: '100px', height: '110px' }}>
              <Stack
                sx={{
                  height: '100%',
                  width: '100%',
                  backgroundColor: activeItem.index === data.index ? '#0068AE' : '#FFFFFF',
                  transition: 'background-color 0.3s, color 0.3s'
                }}
              >
                <Stack
                  sx={{
                    transition: 'background-color 0.3s, color 0.3s',
                    backgroundColor: activeItem.index === data.index ? '#FFFFFF' : '#E4E4E4',
                    borderRadius: '50%',
                    height: '41px',
                    width: '41px',

                    mx: 'auto',
                    diplay: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column',
                    marginTop: '18px'
                  }}
                >
                  <Stack>{data.icon}</Stack>
                  {/* <CardMedia sx={{ width: '24px', height: '24px' }} component="img" image={data.icon} alt="Layer" /> */}
                </Stack>
                <Typography
                  sx={{
                    diplay: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column',
                    marginTop: '15px',

                    color: activeItem.index === data.index ? '#FFF' : '#000',
                    fontSize: '10px',
                    fontWeight: 500,
                    textAlign: 'center',
                    display: 'block'
                  }}
                >
                  {data.title}
                </Typography>
              </Stack>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};
export default VerificationProgress;
