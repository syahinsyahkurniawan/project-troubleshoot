import styled from '@emotion/styled';

import { Box } from '@mui/system';
import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

const ProgressCard = (props) => {
  const { icon, title } = props;

  return (
    <Card sx={{ maxWidth: '100px', maxHeight: '110px' }}>
      <Box>{icon}</Box>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {title}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default ProgressCard;
