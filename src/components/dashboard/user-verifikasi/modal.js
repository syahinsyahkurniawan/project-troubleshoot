import {
  Button,
  CardContent,
  FormControl,
  FormControlLabel,
  FormLabel,
  IconButton,
  InputLabel,
  MenuItem,
  Modal,
  Radio,
  RadioGroup,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography
} from '@mui/material';
import MainCard from 'components/ui-component/cards/MainCard';
import { useVerificationProfileForm } from 'hooks/forms/dashboard/user-verifikasi/useVerificationProfile';
import { useVerifikasi } from 'hooks/mutation/dashboard/useVerifikasi';
import { useRef, useState } from 'react';
import CloseIcon from '@mui/icons-material/Close';
import { Box } from '@mui/system';
import Image from 'next/image';
import { Controller } from 'react-hook-form';

const ModalDetailData = ({ data, close, refresh }) => {
  const rootRef = useRef(null);
  const form = useVerificationProfileForm();
  const { mutateAsync } = useVerifikasi();
  const [show, setShow] = useState(false);
  const uuid = data?.uuid;

  const onSubmit = async (values) => {
    const payload = {
      ...values,
      isVerification: values.isVerification === 'accept'
    };

    mutateAsync({ uuid, data: payload }).then(() => {
      refresh();
      close();
    });
  };

  const handleRadioChange = (e) => {
    const { value } = e.target;

    setShow(value === 'decline');
  };

  return (
    <Modal
      disablePortal
      disableEnforceFocus
      disableAutoFocus
      open
      aria-labelledby="server-modal-title"
      aria-describedby="server-modal-description"
      sx={{
        display: 'flex',
        p: 1,
        alignItems: 'center',
        justifyContent: 'center'
      }}
      container={() => rootRef.current}
    >
      <MainCard
        sx={{
          width: `"60%"`,
          zIndex: 1
        }}
        title="Detail User Verifikasi"
        content={false}
        secondary={
          <IconButton size="large" onClick={close}>
            <CloseIcon fontSize="small" />
          </IconButton>
        }
      >
        <CardContent>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              gridColumn: '25px',
              columnGap: '25px',
              overflow: 'auto'
            }}
          >
            <TableContainer>
              <Table sx={{ minWidth: 350 }} aria-label="simple table">
                <TableBody>
                  <TableRow hover key="name">
                    <TableCell>Nama</TableCell>
                    <TableCell align="right">{data?.nama}</TableCell>
                  </TableRow>
                  <TableRow hover key="email">
                    <TableCell>Email</TableCell>
                    <TableCell align="right">{data?.email}</TableCell>
                  </TableRow>
                  <TableRow hover key="alamat">
                    <TableCell>Alamat</TableCell>
                    <TableCell align="right">{data?.alamat}</TableCell>
                  </TableRow>
                  <TableRow hover key="villages">
                    <TableCell>Kelurahan / Desa</TableCell>
                    <TableCell align="right">{data?.villages?.name}</TableCell>
                  </TableRow>
                  <TableRow hover key="district">
                    <TableCell>Kecamatan</TableCell>
                    <TableCell align="right">{data?.districts?.name}</TableCell>
                  </TableRow>
                  <TableRow hover key="regencies">
                    <TableCell>Kota / Kabupaten</TableCell>
                    <TableCell align="right">{data?.regencies?.name}</TableCell>
                  </TableRow>
                  <TableRow hover key="provices">
                    <TableCell>Provinsi</TableCell>
                    <TableCell align="right">{data?.provices?.name}</TableCell>
                  </TableRow>
                  <TableRow hover key="no_hp">
                    <TableCell>No Handphone</TableCell>
                    <TableCell align="right">{data?.handpone}</TableCell>
                  </TableRow>
                  <TableRow hover key="no_ktp">
                    <TableCell>NO KTP</TableCell>
                    <TableCell align="right">{data?.noKtp}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <Box>
              <h4>Foto KTP</h4>
              <div>
                <Image src={data?.photo} width={400} height={400} />
              </div>
            </Box>
            <Box>
              <h4>Foto Selfie & KTP</h4>
              <div>
                <Image src={data?.uploadSelfie} width={400} height={400} />
              </div>
            </Box>
          </Box>
          {!data?.isVerification ? (
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row'
              }}
            >
              <form onSubmit={form.handleSubmit(onSubmit)} {...form}>
                <Controller
                  control={form.control}
                  name="isVerification"
                  render={({ field: { onChange, onBlur, value, name, ref } }) => (
                    <>
                      <FormLabel htmlFor="isVerification">Status Verifikasi</FormLabel>
                      <RadioGroup
                        row
                        aria-labelledby="demo-row-radio-buttons-group-label"
                        onChange={(e) => {
                          onChange(e);
                          handleRadioChange(e);
                        }}
                        name={name}
                        value={value}
                        ref={ref}
                        onBlur={onBlur}
                      >
                        <FormControlLabel value="accept" control={<Radio />} label="Disetujui" />
                        <FormControlLabel value="decline" control={<Radio />} label="Tidak Disetujui" />
                      </RadioGroup>
                    </>
                  )}
                />
                {form?.formState.errors && <Typography color="error">{form?.formState?.errors?.isVerification?.message}</Typography>}
                {show ? (
                  <Controller
                    control={form.control}
                    name="keterangan"
                    render={({ field }) => (
                      <>
                        <FormControl fullWidth>
                          <InputLabel id="keterangan-verification">Keterangan</InputLabel>
                          <Select
                            labelId="keterangan-verification"
                            id="demo-simple-select"
                            disabled={data?.isVerification}
                            defaultValue={data?.keterangan}
                            {...field}
                          >
                            <MenuItem value="Foto KTP Blur">Foto KTP Blur</MenuItem>
                            <MenuItem value="Foto Selfie Blur">Foto Selfie Blur</MenuItem>
                            <MenuItem value="Foto Buku Rekening Blur">Foto Buku Rekening Blur</MenuItem>
                            <MenuItem value="Data Tidak Sesuai">Data Tidak Sesuai</MenuItem>
                            <MenuItem value="Data Tidak Lengkap">Data Tidak Lengkap</MenuItem>
                          </Select>
                        </FormControl>
                      </>
                    )}
                  />
                ) : null}
                <Button
                  color="secondary"
                  variant="contained"
                  sx={{
                    margin: '0.4rem'
                  }}
                  type="submit"
                  disabled={data?.isVerification}
                >
                  Submit Data
                </Button>
              </form>
            </Box>
          ) : null}
        </CardContent>
      </MainCard>
    </Modal>
  );
};
export default ModalDetailData;
