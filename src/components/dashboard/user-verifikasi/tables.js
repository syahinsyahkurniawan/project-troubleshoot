import {
  Alert,
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel
} from '@mui/material';

import MainCard from 'components/ui-component/cards/MainCard';
import { useProfilesVerfication } from 'hooks/queries/user-verifikasi/useUserVerfikasi';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/system';
import { visuallyHidden } from '@mui/utils';
import { IconSearch } from '@tabler/icons';

import ModalDetailData from 'components/dashboard/user-verifikasi/modal';
// table data
function createData(name, email, address, NoHp, NoKTP, statusVerification, uuid) {
  return {
    name,
    email,
    address,
    NoHp,
    NoKTP,
    statusVerification,
    uuid
  };
}
// table header
const headCellsData = [
  {
    id: 'name',
    numeric: true,
    disablePadding: true,
    label: 'Nama'
  },
  {
    id: 'email',
    numeric: true,
    disablePadding: true,
    label: 'Email'
  },
  {
    id: 'address',
    numeric: true,
    disablePadding: true,
    label: 'Alamat'
  },
  {
    id: 'NoHp',
    numeric: true,
    disablePadding: true,
    label: 'No. Handphone'
  },
  {
    id: 'NoKTP',
    numeric: true,
    disablePadding: true,
    label: 'No. KTP'
  },
  {
    id: 'statusVerification',
    numeric: true,
    disablePadding: true,
    label: 'Status Verifikasi'
  },
  {
    id: 'uuid',
    numeric: true,
    disablePadding: true,
    label: 'Aksi'
  }
];
function EnhancedTableHead({ order, orderBy, numSelected, rowCount, onRequestSort }) {
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox" sx={{ pl: 3 }}></TableCell>
        {headCellsData.map((headCell) => (
          <TableCell key={headCell.id} sortDirection={orderBy === headCell.id ? order : false}>
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};
// table filter

function UserVerifikasiTable() {
  const [verifData, setVerifData] = useState([]);
  const { data, refetch } = useProfilesVerfication();
  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    setTableData(data);
  }, [data, refetch]);

  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('name');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [modalOpen, setModalOpen] = useState(false);
  const [modalData, setModalData] = useState({});

  const handleModal = (data) => {
    console.log(data, 'data');
    setModalData(data);
    setModalOpen(true);
  };
  const closeModal = () => {
    setModalData({});
    setModalOpen(false);
  };
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event?.target.value, 10));
    setPage(0);
  };

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - verifData.length) : 0;
  // const [isLoadingPage, setLoadingPage] = React.useState(true);
  // React.useEffect(() => {
  //   setLoadingPage(false);
  // }, []);
  return (
    <>
      <MainCard content={false} title="Data Verifkasi User">
        <Paper sx={{ width: '100%', mb: 2 }}>
          {/* table */}
          <TableContainer>
            <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle" size={dense ? 'small' : 'medium'}>
              <EnhancedTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onRequestSort={handleRequestSort}
                rowCount={verifData.length}
              />
              <TableBody>
                {tableData?.data?.map((row, index) => (
                  <TableRow key={index}>
                    <TableCell>{row.id}</TableCell>
                    <TableCell>{row.nama}</TableCell>
                    <TableCell>{row.email}</TableCell>
                    <TableCell>{row.alamat}</TableCell>
                    <TableCell>{row.handpone}</TableCell>
                    <TableCell>{row.noKtp}</TableCell>
                    <TableCell>
                      <Alert severity={row.isVerification ? 'success' : 'warning'}>
                        {row.isVerification ? 'Terverifikasi' : 'Pending Verifikasi'}
                      </Alert>
                    </TableCell>
                    <TableCell>
                      <Button variant="contained" onClick={() => handleModal(row)} startIcon={<IconSearch />}>
                        Detail Data
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
                {emptyRows > 0 && (
                  <TableRow
                    style={{
                      height: (dense ? 33 : 53) * emptyRows
                    }}
                  >
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>

          {/* table data */}
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={verifData.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </MainCard>
      {modalOpen && <ModalDetailData data={modalData} close={closeModal} refresh={refetch} />}
    </>
  );
}

export default UserVerifikasiTable;
