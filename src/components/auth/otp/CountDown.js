/* eslint-disable consistent-return */
import { Typography } from '@mui/material';
import { Box } from '@mui/system';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

function CountdownTimer(props) {
  const { restart } = props;
  const initialTime = 90; // 1 minute and 30 seconds
  const [time, setTime] = useState(initialTime);

  useEffect(() => {
    if (time > 0) {
      const timer = setInterval(() => {
        setTime((prevTime) => prevTime - 1);
      }, 1000);

      return () => {
        clearInterval(timer);
      };
    }
  }, [time, restart]);

  const formatTime = (seconds) => {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
  };

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
      <Typography variant="body1">{formatTime(time)}</Typography>
    </Box>
  );
}
CountdownTimer.propTypes = {
  restart: PropTypes.bool
};

export default CountdownTimer;
