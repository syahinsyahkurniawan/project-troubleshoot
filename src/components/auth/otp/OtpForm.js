// material-ui
import { useTheme } from '@mui/material/styles';
import { Button, CircularProgress, Grid, Stack, Typography } from '@mui/material';

// third-party
import OtpInput from 'react18-input-otp';

import { useOtpForm } from 'hooks/forms/auth/useOtpForm';

import { useCountDown } from 'hooks/utils/useCountDown';
import { Box } from '@mui/system';

const OtpForm = () => {
  const theme = useTheme();
  const { otp, onSubmit, isError, isDisabled, isLoading, onChangeOtp, onResenOtp, isSuccess, resendLoading } = useOtpForm();
  const { formatTime, time, setTime } = useCountDown();

  return (
    <Grid container spacing={3} justifyContent="center">
      <Grid item xs={12}>
        <OtpInput
          numInputs={4}
          value={otp}
          onChange={onChangeOtp}
          containerStyle={{ justifyContent: 'center' }}
          inputStyle={{
            width: '57px',
            margin: '8px',
            padding: '10px',
            border: `1px solid #000`,
            height: '57px',
            borderRadius: 4,
            ':hover': {
              borderColor: theme.palette.primary.main
            }
          }}
          focusStyle={{
            outline: 'none',
            border: `2px solid ${theme.palette.primary.main}`
          }}
          shouldAutoFocus
          errorStyle={{
            border: `2px solid ${theme.palette.error.main}`
          }}
          hasErrored={!!isError}
          isDisabled={isLoading || resendLoading}
        />
      </Grid>
      <Grid items container xs={12} justifyContent="center">
        {isError && <Typography color={theme.palette.error.main}>Kode OTP salah</Typography>}
      </Grid>
      <Grid items container xs={12}>
        <Box sx={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
          <Typography variant="body1">{formatTime(time)}</Typography>
        </Box>
      </Grid>
      <Grid item xs={6} md={12}>
        <Button
          fullWidth
          size="large"
          type="button"
          variant="contained"
          onClick={onSubmit}
          disabled={isDisabled || isLoading || time === 0 || resendLoading}
        >
          {isLoading ? <CircularProgress aria-label="progress" /> : 'Verifikasi'}
        </Button>
      </Grid>
      {time < 10 && (
        <Grid item xs={12}>
          <Stack direction="row" alignItems="center" gap={1} justifyContent="center">
            <Typography>Tidak dapat kode?</Typography>
            <Button
              disabled={resendLoading}
              variant="text"
              sx={{ padding: 0 }}
              onClick={() => {
                onResenOtp().then(() => {
                  if (isSuccess) {
                    setTime(90);
                  }
                });
              }}
            >
              Kirim ulang
            </Button>
          </Stack>
        </Grid>
      )}
    </Grid>
  );
};
export default OtpForm;
