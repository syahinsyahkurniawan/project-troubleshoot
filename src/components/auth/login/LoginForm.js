/* eslint-disable react/prop-types */
import styled from '@emotion/styled';
import { Visibility } from '@mui/icons-material';
import { Button, Grid, IconButton, InputAdornment, InputLabel, Stack, TextField, Typography } from '@mui/material';
import { Box } from '@mui/system';
import Link from 'Link';
import { useLoginForm } from 'hooks/forms/auth/useLoginForm';
import { useLogin } from 'hooks/mutation/auth/useLogin';
import { useResendOtp } from 'hooks/mutation/auth/useResendOTP';

import React, { Fragment, useState } from 'react';
import { Controller } from 'react-hook-form';
import { gridSpacing } from 'store/constant';
import EyeClosed from 'svgs/EyeClosed';

const StyledButton = styled(Button)(() => ({
  padding: '10px 70px',
  height: '48px',
  fontWeight: 500,
  boxShadow: 'none',
  fontSize: '14px'
}));

const LoginForm = () => {
  const form = useLoginForm();
  const { mutate, isError, error } = useLogin();
  const { mutateAsync: resend } = useResendOtp();

  const [showPassword, setShowPassword] = useState(false);

  const onSubmit = async (values) => {
    mutate(values);
  };

  const onErrorNotVerify = () => {
    const email = form.getValues('email');
    resend({ email })
      .then((res) => {
        console.log(res);
        window.open(`/auth/register/otp?email=${email}`);
      })
      .catch((err) => {
        throw err;
      });
  };

  return (
    <>
      {isError && (
        <Grid item xs={12}>
          <Box sx={{ width: '100%', bgcolor: 'error.light', padding: '15px 23px' }}>
            <Typography color="error.main" variant="body1">
              {error?.response?.status === 403 ? (
                <Box sx={{ display: 'flex', gap: '4px' }}>
                  <>{`${error?.response?.data?.message}. Klik `}</>
                  <Typography color="primary.main" sx={{ textDecoration: 'underline', cursor: 'pointer' }} onClick={onErrorNotVerify}>
                    disini
                  </Typography>
                  untuk verifikasi akun anda
                </Box>
              ) : (
                'Email atau Password Anda Salah!'
              )}
            </Typography>
          </Box>
        </Grid>
      )}

      <Grid item xs={12}>
        <form onSubmit={form.handleSubmit(onSubmit)} {...form}>
          <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
              <Controller
                control={form.control}
                name="email"
                render={({ field }) => (
                  <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
                    <InputLabel>Email</InputLabel>
                    <TextField
                      fullWidth
                      type="text"
                      placeholder="Enter Your Email"
                      error={form?.formState.errors.email && Boolean(form?.formState?.errors?.email)}
                      helperText={form?.formState.errors ? form?.formState?.errors?.email?.message : null}
                      {...field}
                    />
                  </Box>
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                control={form.control}
                name="password"
                render={({ field }) => (
                  <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
                    <InputLabel>Password</InputLabel>
                    <TextField
                      fullWidth
                      type={showPassword ? 'text' : 'password'}
                      placeholder="Enter Your Password"
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton onClick={() => setShowPassword(!showPassword)}>
                              {showPassword ? <Visibility /> : <EyeClosed width="23px" height="23px" />}
                            </IconButton>
                          </InputAdornment>
                        )
                      }}
                      error={form?.formState.errors.password && Boolean(form?.formState?.errors?.password)}
                      helperText={form?.formState.errors ? form?.formState?.errors?.password?.message : null}
                      {...field}
                    />
                  </Box>
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography
                variant="subtitle1"
                sx={{ textDecoration: 'none' }}
                component={Link}
                href="/auth/forgot-password"
                color="primary.800"
              >
                Lupa Password?
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Stack direction="row" justifyContent="flex-start">
                <StyledButton type="submit" variant="contained" color="primary">
                  Masuk
                </StyledButton>
              </Stack>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </>
  );
};

export default LoginForm;
