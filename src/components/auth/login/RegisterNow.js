import { useTheme } from '@emotion/react';
import { Grid, Typography } from '@mui/material';
import Link from 'Link';
import React from 'react';

const RegisterNow = () => {
  const theme = useTheme();
  return (
    <Grid item xs={12}>
      <div style={{ display: 'flex', flexDirection: 'row', gap: '4px', alignItems: 'center' }}>
        <Typography variant="subtitle1">Belum punya Akun?</Typography>
        <Typography
          variant="subtitle1"
          sx={{ textDecoration: 'none' }}
          component={Link}
          href="/auth/register"
          color={theme.palette.primary[800]}
        >
          Daftar Sekarang
        </Typography>
      </div>
    </Grid>
  );
};

export default RegisterNow;
