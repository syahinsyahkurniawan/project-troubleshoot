/* eslint-disable react/prop-types */

import styled from '@emotion/styled';
import { Visibility } from '@mui/icons-material';
import { Button, Grid, IconButton, InputAdornment, InputLabel, Stack, TextField } from '@mui/material';
import MuiTooltip, { tooltipClasses } from '@mui/material/Tooltip';

import { Box } from '@mui/system';
import KeyIcon from '@mui/icons-material/Key';

import { useResetForm } from 'hooks/forms/auth/useResetForm';
import React, { useState } from 'react';
import { Controller } from 'react-hook-form';
import { gridSpacing } from 'store/constant';
import EyeClosed from 'svgs/EyeClosed';
import { IconInfoCircle } from '@tabler/icons';
import { useTheme } from '@emotion/react';

const StyledButton = styled(Button)(() => ({
  padding: '10px 70px',
  height: '48px',
  fontWeight: 500,
  boxShadow: 'none',
  fontSize: '14px'
}));

const HtmlTooltip = styled(({ className, ...other }) => <MuiTooltip {...other} classes={{ popper: className }} />)(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#fff',
    color: '#000',
    fontSize: theme.typography.pxToRem(12),
    borderRadius: ' 5px',
    border: '1px solid #2EABFF'
  }
}));

const ResetForm = () => {
  const form = useResetForm();
  const theme = useTheme();

  const [showPassword, setShowPassword] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);

  return (
    <Grid item xs={12}>
      <form {...form}>
        <Grid container spacing={gridSpacing}>
          <Grid container item xs={12} spacing={2}>
            <Grid item xs={12} md={12}>
              <Controller
                control={form.control}
                name="password"
                render={({ field }) => (
                  <>
                    <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
                      <InputLabel>
                        Password
                        <Tooltip theme={theme} />
                      </InputLabel>
                      <TextField
                        fullWidth
                        type={showPassword ? 'text' : 'password'}
                        placeholder="Password Baru"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <KeyIcon />
                            </InputAdornment>
                          ),
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={() => setShowPassword(!showPassword)}>
                                {showPassword ? <Visibility /> : <EyeClosed width="23px" height="23px" />}
                              </IconButton>
                            </InputAdornment>
                          )
                        }}
                        error={form?.formState.errors.password && Boolean(form?.formState?.errors?.password)}
                        helperText={form?.formState.errors ? form?.formState?.errors?.password?.message : null}
                        {...field}
                      />
                    </Box>
                  </>
                )}
              />
            </Grid>
            <Grid item xs={12} md={12}>
              <Controller
                control={form.control}
                name="passwordConfirm"
                render={({ field }) => (
                  <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
                    <InputLabel>Konfirmasi Password</InputLabel>
                    <TextField
                      fullWidth
                      type={showConfirm ? 'text' : 'password'}
                      placeholder="Konfirmasi Password"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <KeyIcon />
                          </InputAdornment>
                        ),
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton onClick={() => setShowConfirm(!showConfirm)}>
                              {showConfirm ? <Visibility /> : <EyeClosed width="23px" height="23px" />}
                            </IconButton>
                          </InputAdornment>
                        )
                      }}
                      error={form?.formState.errors.passwordConfirm && Boolean(form?.formState?.errors?.passwordConfirm)}
                      helperText={form?.formState.errors ? form?.formState?.errors?.passwordConfirm?.message : null}
                      {...field}
                    />
                  </Box>
                )}
              />
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Stack direction="row" justifyContent="center">
              <StyledButton type="submit" variant="outlined" color="primary" sx={{ width: '100%' }}>
                Submit
              </StyledButton>
            </Stack>
          </Grid>
        </Grid>
      </form>
    </Grid>
  );
};

export default ResetForm;

const Tooltip = ({ theme }) => (
  <HtmlTooltip
    placement="right-end"
    title={
      <Box sx={{ flex: 'row', paddingX: '12px', margin: 0, textAlign: 'left' }}>
        <p>Ketentuan membuat Password:</p>
        <ul style={{ margin: '0px !important' }}>
          <li style={{ margin: 0 }}>Minimal 10 karakter</li>
          <li style={{ margin: 0 }}>{` Gunakan Kombinasi huruf kapital (Aa), Angka (123) dan Karakter spesial (@,!,.,*)`}</li>
        </ul>
      </Box>
    }
  >
    <IconButton sx={{ width: 'auto', padding: 0 }}>
      <IconInfoCircle color={theme.palette.primary[800]} size={14} />
    </IconButton>
  </HtmlTooltip>
);
