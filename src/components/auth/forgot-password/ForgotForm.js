/* eslint-disable react/prop-types */
import { useTheme } from '@emotion/react';
import styled from '@emotion/styled';

import { Button, Grid, InputLabel, Stack, TextField, Typography, useMediaQuery } from '@mui/material';
import { Box } from '@mui/system';

import { useForgotForm } from 'hooks/forms/auth/useForgotForm';
import React from 'react';
import { Controller } from 'react-hook-form';
import { gridSpacing } from 'store/constant';

const StyledButton = styled(Button)(() => ({
  padding: '10px 70px',
  height: '48px',
  fontWeight: 500,
  boxShadow: 'none',
  fontSize: '14px'
}));

const ForgotForm = () => {
  const form = useForgotForm();
  const theme = useTheme();

  const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <>
      <Grid container alignItems="center" justifyContent="center" textAlign="center" spacing={2} item xs={12}>
        <Stack item xs={12} gap={1}>
          <Typography gutterBottom variant={matchDownSM ? 'h3' : 'h2'}>
            Lupa Password
          </Typography>
          <Typography sx={{ fontSize: '16px', fontWeight: 400 }} variant="caption" fontSize="16px" textAlign="center">
            Masukan Email yang kamu daftarkan sebelumnya
          </Typography>
        </Stack>
      </Grid>

      <Grid item xs={12}>
        <form {...form}>
          <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
              <Controller
                control={form.control}
                name="email"
                render={({ field }) => (
                  <Box sx={{ display: 'flex', gap: '10px', flexDirection: 'column' }}>
                    <InputLabel>Email</InputLabel>
                    <TextField
                      fullWidth
                      type="text"
                      placeholder="Email kamu"
                      error={form?.formState.errors.email && Boolean(form?.formState?.errors?.email)}
                      helperText={form?.formState.errors ? form?.formState?.errors?.email?.message : null}
                      {...field}
                    />
                  </Box>
                )}
              />
            </Grid>

            <Grid item xs={12}>
              <Stack direction="row" justifyContent="center">
                <StyledButton type="submit" variant="outlined" color="primary" sx={{ width: '100%' }}>
                  Verifikasi
                </StyledButton>
              </Stack>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </>
  );
};

export default ForgotForm;
