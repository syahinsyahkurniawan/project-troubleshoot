/* eslint-disable react/prop-types */

import styled from '@emotion/styled';
import { Visibility } from '@mui/icons-material';
import {
  Button,
  Checkbox,
  Divider,
  FormControlLabel,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  Stack,
  TextField,
  Typography
} from '@mui/material';
import MuiTooltip, { tooltipClasses } from '@mui/material/Tooltip';

import { Box } from '@mui/system';
import Link from 'Link';

import { useRegisterForm } from 'hooks/forms/auth/useRegisterForm';
import React, { useState } from 'react';
import { Controller } from 'react-hook-form';
import { gridSpacing } from 'store/constant';
import EyeClosed from 'svgs/EyeClosed';
import { IconInfoCircle } from '@tabler/icons';
import { useTheme } from '@emotion/react';

const StyledButton = styled(Button)(() => ({
  padding: '10px 70px',
  height: '48px',
  fontWeight: 500,
  boxShadow: 'none',
  fontSize: '14px'
}));

const HtmlTooltip = styled(({ className, ...other }) => <MuiTooltip {...other} classes={{ popper: className }} />)(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#fff',
    color: '#000',
    fontSize: theme.typography.pxToRem(12),
    borderRadius: ' 5px',
    border: '1px solid #2EABFF'
  }
}));

const RegisterForm = () => {
  const form = useRegisterForm();
  const theme = useTheme();

  const [showPassword, setShowPassword] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);

  return (
    <Grid item xs={12}>
      {form?.error && (
        <Grid item xs={12} sx={{ paddingBottom: '10pxw' }}>
          <Box sx={{ width: '100%', bgcolor: 'error.light', padding: '15px 23px' }}>
            {form?.error?.response?.data?.message?.map((message) => (
              <Typography color="error.main" variant="body1">
                {message.toUpperCase()}
              </Typography>
            ))}
          </Box>
        </Grid>
      )}

      <Grid item xs={12}>
        <form onSubmit={form.onSubmit} {...form}>
          <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
              <Controller
                control={form.control}
                name="fullname"
                render={({ field }) => (
                  <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
                    <InputLabel>Nama Lengkap</InputLabel>
                    <TextField
                      required
                      fullWidth
                      type="text"
                      placeholder="Nama Lengkap"
                      error={form?.formState.errors.fullname && Boolean(form?.formState?.errors?.fullname)}
                      helperText={form?.formState.errors ? form?.formState?.errors?.fullname?.message : null}
                      {...field}
                    />
                  </Box>
                )}
              />
            </Grid>
            <Grid container item xs={12} spacing={2}>
              <Grid item xs={12} md={6}>
                <Controller
                  control={form.control}
                  name="mobile"
                  render={({ field }) => (
                    <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
                      <InputLabel required>No.Handphone</InputLabel>
                      <TextField
                        required
                        fullWidth
                        type="text"
                        placeholder="Phone number"
                        InputProps={{
                          startAdornment: (
                            <>
                              <InputAdornment position="start">+62</InputAdornment>
                              <Divider sx={{ height: 28, m: 0.5, mr: 1.5 }} orientation="vertical" />
                            </>
                          )
                        }}
                        error={form?.formState.errors.mobile && Boolean(form?.formState?.errors?.mobile)}
                        helperText={form?.formState.errors ? form?.formState?.errors?.mobile?.message : null}
                        {...field}
                      />
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Controller
                  control={form.control}
                  name="email"
                  render={({ field }) => (
                    <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
                      <InputLabel required>Email</InputLabel>
                      <TextField
                        required
                        fullWidth
                        type="email"
                        placeholder="johndoe@mail.com"
                        error={form?.formState.errors.email && Boolean(form?.formState?.errors?.email)}
                        helperText={form?.formState.errors ? form?.formState?.errors?.email?.message : null}
                        {...field}
                      />
                    </Box>
                  )}
                />
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={2}>
              <Grid item xs={12} md={6}>
                <Controller
                  control={form.control}
                  name="password"
                  render={({ field }) => (
                    <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
                      <Box sx={{ display: 'flex' }} flexDirection="row" alignItems="center" gap={1}>
                        <InputLabel>Password</InputLabel>
                        <HtmlTooltip
                          placement="right-end"
                          title={
                            <Box sx={{ flex: 'row', paddingX: '12px', margin: 0, textAlign: 'left' }}>
                              <p>Ketentuan membuat Password:</p>
                              <ul style={{ margin: '0px !important' }}>
                                <li style={{ margin: 0 }}>Minimal 10 karakter</li>
                                <li style={{ margin: 0 }}>
                                  {` Gunakan Kombinasi huruf kapital (Aa), Angka (123) dan Karakter spesial (@,!,.,*)`}
                                </li>
                              </ul>
                            </Box>
                          }
                        >
                          <IconButton sx={{ width: 'auto', padding: 0 }}>
                            <IconInfoCircle color={theme.palette.primary[800]} size={14} />
                          </IconButton>
                        </HtmlTooltip>
                      </Box>
                      <TextField
                        fullWidth
                        type={showPassword ? 'text' : 'password'}
                        placeholder="Enter Your Password"
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={() => setShowPassword(!showPassword)}>
                                {showPassword ? <Visibility /> : <EyeClosed width="23px" height="23px" />}
                              </IconButton>
                            </InputAdornment>
                          )
                        }}
                        error={form?.formState.errors.password && Boolean(form?.formState?.errors?.password)}
                        helperText={form?.formState.errors ? form?.formState?.errors?.password?.message : null}
                        {...field}
                      />
                    </Box>
                  )}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <Controller
                  control={form.control}
                  name="passwordConfirm"
                  render={({ field }) => (
                    <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
                      <InputLabel>Confirm Password</InputLabel>
                      <TextField
                        fullWidth
                        type={showConfirm ? 'text' : 'password'}
                        placeholder="Enter Your Password"
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton onClick={() => setShowConfirm(!showConfirm)}>
                                {showConfirm ? <Visibility /> : <EyeClosed width="23px" height="23px" />}
                              </IconButton>
                            </InputAdornment>
                          )
                        }}
                        error={form?.formState.errors.passwordConfirm && Boolean(form?.formState?.errors?.passwordConfirm)}
                        helperText={form?.formState.errors ? form?.formState?.errors?.passwordConfirm?.message : null}
                        {...field}
                      />
                    </Box>
                  )}
                />
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Controller
                control={form.control}
                name="agree"
                render={({ field }) => (
                  <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 0 }}>
                    <FormControlLabel
                      sx={{ padding: 0, marginRight: '4px' }}
                      control={<Checkbox defaultChecked={false} {...field} />}
                      label={
                        <div style={{ display: 'flex', flexDirection: 'row', gap: '4px', alignItems: 'center', flexWrap: 'wrap' }}>
                          <Typography variant="subtitle1" color="grey.500">
                            Saya setuju dengan
                          </Typography>
                        </div>
                      }
                    />
                    <Typography
                      variant="subtitle1"
                      sx={{ textDecoration: 'none', cursor: 'pointer' }}
                      href="/terms-and-condition"
                      color="primary.800"
                      onClick={() => {
                        window.open('/terms-and-condition', '_blank');
                      }}
                    >
                      Syarat dan Ketentuan Completed
                    </Typography>
                  </Box>
                )}
              />
              {form?.formState.errors && <Typography color="error">{form?.formState?.errors?.agree?.message}</Typography>}
            </Grid>
            <Grid item xs={12}>
              <div style={{ display: 'flex', flexDirection: 'row', gap: '4px', alignItems: 'center' }}>
                <Typography variant="subtitle1">Sudah punya Akun?</Typography>
                <Typography variant="subtitle1" sx={{ textDecoration: 'none' }} component={Link} href="/auth/login" color="primary.800">
                  Masuk
                </Typography>
              </div>
            </Grid>
            <Grid item xs={12}>
              <Stack direction="row" justifyContent="flex-start">
                <StyledButton type="submit" variant="contained" color="primary">
                  Daftar
                </StyledButton>
              </Stack>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Grid>
  );
};

export default RegisterForm;
