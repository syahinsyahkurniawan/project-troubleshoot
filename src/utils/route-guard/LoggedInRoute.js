import Loader from 'components/ui-component/Loader';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';

const LoggedInRoute = ({ children }) => {
  const router = useRouter();
  const { access_token: jwt } = useSelector((s) => s?.profile?.data);
  console.log(jwt);

  useEffect(() => {
    if (jwt) {
      router.push('/dashboard');
    }
    // eslint-disable-next-line
    console.log(jwt);
  }, [jwt, router]);

  if (jwt) return <Loader />;

  return children;
};
export default LoggedInRoute;
