import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { useRouter } from 'next/router';

// project imports
// import Loader from 'components/ui-component/Loader';
import { useCheckJwt } from 'hooks/utils/useCheckJwt';
// import { useProfile } from 'hooks/queries/auth/useProfile';

/**
 * Authentication guard for routes
 * @param {PropTypes.node} children children element/node
 */
const PrivateRoute = ({ children }) => {
  const { jwt, uuid } = useCheckJwt();
  const router = useRouter();

  useEffect(() => {
    if (!jwt) {
      router.push('/auth/login');
    }
  }, [uuid, jwt]);

  // if (isLoading) return <Loader />;

  return children;
};

PrivateRoute.propTypes = {
  children: PropTypes.node
};

export default PrivateRoute;
