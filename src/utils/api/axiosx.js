import axios from 'axios';

export const axiosx = (auth) => {
  const instance = axios.create();

  instance.interceptors.request.use(
    async (config) => {
      if (auth) {
        const token = getToken();
        if (!token) return config;

        config.headers.Authorization = `Bearer ${token}`;
      }

      return config;
    },
    (error) => Promise.reject(error)
  );

  return instance;
};

const getToken = () => {
  const jwt = localStorage.getItem('jwt') ?? '';

  return jwt;
};
