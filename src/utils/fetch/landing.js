import { mainAPI } from 'api/config';
import axios from 'axios';

const apiUrl = mainAPI;

export const landingData = async () => {
  const resp = await axios.get(`${apiUrl}/landing`);
  // console.log('🚀 ~ file: landing.js:6 ~ landingData ~ resp:', resp);
  return resp;
};
