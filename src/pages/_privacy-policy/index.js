// material-ui

import LAYOUT from 'constant';
import Layout from 'layout';
import Loadable from 'components/ui-component/Loadable';
import { lazy } from 'react';

const PrivacyPolicyContent = Loadable(lazy(() => import('content/privacy-policy')));

const PrivacyPolicy = () => <PrivacyPolicyContent />;

PrivacyPolicy.getLayout = function getLayout(page) {
  return (
    <Layout variant={LAYOUT.landing} titlePage="Syarat dan Ketentuan">
      {page}
    </Layout>
  );
};

export default PrivacyPolicy;
