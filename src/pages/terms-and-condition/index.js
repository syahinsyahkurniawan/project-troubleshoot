// material-ui

import LAYOUT from 'constant';
import Layout from 'layout';
import Loadable from 'components/ui-component/Loadable';
import { lazy } from 'react';

const TermAndConditionsContent = Loadable(lazy(() => import('content/tnc')));

const TermAndConditions = () => <TermAndConditionsContent />;

TermAndConditions.getLayout = function getLayout(page) {
  return (
    <Layout variant={LAYOUT.landing} titlePage="Syarat dan Ketentuan">
      {page}
    </Layout>
  );
};

export default TermAndConditions;
