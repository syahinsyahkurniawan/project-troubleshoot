import Loadable from 'components/ui-component/Loadable';
import { lazy } from 'react';

const ForgotPage = Loadable(lazy(() => import('content/auth/ForgotPage')));

const Forgot = () => <ForgotPage />;

export default Forgot;
