import Loadable from 'components/ui-component/Loadable';
import { lazy } from 'react';

const RegisterPage = Loadable(lazy(() => import('content/auth/RegisterPage')));

const Register = () => <RegisterPage />;

export default Register;
