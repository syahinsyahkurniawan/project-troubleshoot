import Loadable from 'components/ui-component/Loadable';
import { lazy } from 'react';

const OtpPage = Loadable(lazy(() => import('content/auth/OtpPage')));

const Otp = () => <OtpPage />;
export default Otp;
