import Loadable from 'components/ui-component/Loadable';
import { lazy } from 'react';

const LoginPage = Loadable(lazy(() => import('content/auth/LoginPage')));

const Login = () => <LoginPage />;

export default Login;
