import Loadable from 'components/ui-component/Loadable';
import { lazy } from 'react';

const ResetPasswordConetent = Loadable(lazy(() => import('content/auth/ResetPassword')));
const ResetPasswordPage = () => <ResetPasswordConetent />;

export default ResetPasswordPage;
