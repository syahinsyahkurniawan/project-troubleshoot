import Layout from 'layout';

import VerifikasiKTPFormWizard from 'components/lengkapi-profile/UploadKTP/VerifikasiKTPFormWizard';
import LAYOUT from 'constant';

const VerifikasiKTP = () => <VerifikasiKTPFormWizard />;

VerifikasiKTP.getLayout = function getLayout(page) {
  return (
    <Layout titlePage="Profile" descPage="Kamu bisa melihat profil kamu disini" variant={LAYOUT.dashboard}>
      {page}
    </Layout>
  );
};

export default VerifikasiKTP;
