import Loadable from 'components/ui-component/Loadable';
import { lazy } from 'react';
import Layout from 'layout';

import LAYOUT from 'constant';

const UserVerifikasiPages = Loadable(lazy(() => import('content/dashboard/user-verifikasi')));
const UserVerifikasi = () => <UserVerifikasiPages />;

UserVerifikasi.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.dashboard}>{page}</Layout>;
};

export default UserVerifikasi;
