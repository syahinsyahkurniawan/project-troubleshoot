// material-ui
import { Alert, Button, Grid, Typography } from '@mui/material';

// project imports
import Layout from 'layout';
import Page from 'components/ui-component/Page';

import { gridSpacing } from '../../store/constant';
import Image from 'next/image';
import { Box } from '@mui/system';

import { useRouter } from 'next/router';
import LAYOUT from 'constant';
import { useSelector } from 'react-redux';
import VerificationProgress from 'components/dashboard/verification-progress/index';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import ProfileVerificationProgress from 'svgs/ProfileVerificationProgress';
import Balance from 'components/dashboard/balance';
import BalanceUpdate from 'components/dashboard/balance-update';
const AlertKTPIcon = () => (
  <Box
    sx={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '26px',
      height: '26px',
      backgroundColor: '#ffc050',
      borderRadius: '50%'
    }}
  >
    <Image src="/assets/images/icons/alert-icon.svg" alt="Alert Icon" width={12} height={12} />
  </Box>
);

const Dashboard = () => {
  const router = useRouter();
  const {
    data: { detail }
  } = useSelector((state) => state?.profile);

  const handleRefresh = () => {
    router.push('/dashboard/verifikasi-ktp');
  };

  return (
    <Page title="Dashboard">
      <Grid container spacing={gridSpacing}>
        <Grid item xs={12}>
          {!detail?.profile?.isVerification && (
            <Alert variant="filled" sx={{ mb: 4, bgcolor: '#fff0d6' }} icon={<AlertKTPIcon />} severity="warning">
              Lengkapi profil kamu dan verrifikasi terlebih dahulu, sebelum memulai pengiriman barang
              {/* <Link href="/verifikasi-ktp"> */}
              <Button onClick={handleRefresh} variant="outlined" color="warning" style={{ marginLeft: '16px', padding: '8px 24px' }}>
                Lengkapi Sekarang
              </Button>
              {/* </Link> */}
            </Alert>
          )}
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <VerificationProgress />
        </Grid>
        <Typography>ini tulisan</Typography>
      </Grid>
      <Grid container sx={{ justifyContent: 'space-around' }}>
        <Grid item xs={7}>
          <Balance />
        </Grid>
        <Grid item xs={3}>
          <BalanceUpdate />
        </Grid>
      </Grid>
    </Page>
  );
};

Dashboard.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.dashboard}>{page}</Layout>;
};

export default Dashboard;
