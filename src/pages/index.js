// material-ui
import { styled } from '@mui/material/styles';

// project imports
import LAYOUT from 'constant';
import Layout from 'layout';

import Page from 'components/ui-component/Page';

import Hero from 'components/home/hero/SectionHero';
import Problems from 'components/home/problems';
import Feature from 'components/home/feature';
import WhyUs from 'components/home/why-completed';
import PrimeCRM from 'components/home/prime-crm';
import ProductExellence from 'components/home/product-excellence';
import Flow from 'components/home/flow';
import Testimony from 'components/home/testimony';
import Faqs from 'components/home/faq';

import Footer from 'components/home/footer';
import Header from 'components/home/header';

const HeaderWrapper = styled('div')(({ theme }) => ({
  paddingTop: 30,
  overflowX: 'hidden',
  overflowY: 'clip',
  width: '100vw',
  background:
    theme.palette.mode === 'dark'
      ? theme.palette.background.default
      : `linear-gradient(360deg, ${theme.palette.grey[100]} 1.09%, ${theme.palette.background.paper} 100%)`,
  [theme.breakpoints.down('md')]: {}
}));

const SectionWrapper = styled('div')({
  paddingTop: 100
});

const ContentWrapper = styled('div')(({ theme }) => ({
  position: 'relative',
  display: 'flex',
  flexDirection: 'column',
  background: theme.palette.paper,
  overflowX: 'hidden'
}));

const Landing = () => (
  <Page title="Completed - Platform Pengiriman No.1 untuk Kirim Barang, Mudah dan Terjangkau">
    <HeaderWrapper id="home">
      <Header />
    </HeaderWrapper>
    <ContentWrapper>
      <Hero />
      <Problems />
      <Feature />
      <WhyUs />
      <PrimeCRM />
      <ProductExellence />
      <Flow />
      <Testimony />
      <Faqs />
    </ContentWrapper>
    <SectionWrapper
      sx={{
        backgroundImage: 'url(assets/images/landingpage/footer/footer-bg.png)',
        backgroundSize: { xs: 'contain', lg: 'cover' },
        backgroundRepeat: 'no-repeat',
        pt: { xs: '120px', md: '300px', xl: '400px' }
      }}
    >
      <Footer />
    </SectionWrapper>
  </Page>
);

Landing.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.minimal}>{page}</Layout>;
};

export default Landing;
