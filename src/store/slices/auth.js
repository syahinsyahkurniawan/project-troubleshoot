// third-party
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  data: {
    auth: '',
    detail: ''
  }
};

const auth = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setAuthData: (state, action) => {
      state.data = {
        ...state.data,
        auth: action.payload
      };
    },
    onLogout: () => initialState,
    setUserDetail: (state, action) => {
      state.data = {
        ...state.data,
        detail: action.payload
      };
    }
  }
});

// Reducer
export default auth.reducer;

export const { setAuthData, setUserDetail, onLogout } = auth.actions;
