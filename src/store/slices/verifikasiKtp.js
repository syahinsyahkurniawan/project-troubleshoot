// third-party
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  verifikasiKtp: {}
};

const auth = createSlice({
  name: 'verifikasiKtp',
  initialState,
  reducers: {
    setVerifikasiKtpData: (state, action) => {
      state.data = action.payload;
    }
  }
});

// Reducer
export default auth.reducer;

export const { setVerifikasiKtpData } = auth.actions;
