// redux/postsSlice.js
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchProvince = createAsyncThunk('province/fetchProvince', async () => {
  const response = await fetch('http://localhost:9000/provinsi');
  return response.json();
});

const provinceSlice = createSlice({
  name: 'province',
  initialState: {
    data: [],
    status: 'idle',
    error: null
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchProvince.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchProvince.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.data = action.payload;
      })
      .addCase(fetchProvince.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message;
      });
  }
});

export default provinceSlice.reducer;
