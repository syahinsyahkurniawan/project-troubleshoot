import { configureStore } from '@reduxjs/toolkit';
import provinceReducer from './provinceSlice';

const store = configureStore({
  reducer: {
    province: provinceReducer
  }
});

export default store;
