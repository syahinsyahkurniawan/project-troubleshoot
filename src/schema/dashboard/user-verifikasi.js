import * as yup from 'yup';

export const verificationProfileSchema = yup.object({
  isVerification: yup.string().required('Harus dipilih salah satu!'),
  keterangan: yup.string()
});
