import * as yup from 'yup';

export const verifikasiKtpSchema = yup.object({
  noKtp: yup
    .string()
    .required('No KTP harus diisi')
    .matches(/^[0-9]+$/, 'No KTP hanya terdiri dari angka')
    .length(16, 'No KTP harus 16 digits!'),
  nama: yup.string().required('Nama harus diisi!'),
  nama_bank: yup.string().required('Nama bank rekening harus diisi!'),
  pemilik_rekening: yup.string().required('Nama pemilik rekening harus diisi!'),
  nomor_rekening: yup.string().required('Nomor Rekening harus diisi!'),
  provinsiId: yup.string().required('provinsi harus dipilih'),
  kabupatenId: yup.string().required('kabupaten harus dipilih'),
  wilayahId: yup.string().required('wilayah harus dipilih'),
  jenisKelamin: yup.bool().oneOf([true, false], 'Field must be checked'),
  alamat: yup.string().required('Alamat harus diisi'),
  agree: yup.bool().oneOf([true], 'Persetujuan Harus dicentang'),
  ktp: yup.mixed(),
  buku_rekening: yup.mixed(),
  // .test('fileSize', 'Image size is too large', (value) => {
  //   if (!value) {
  //     return true;
  //   }
  //   return value.size <= 1024000;
  // })
  // .test('fileType', 'Invalid file type', (value) => {
  //   if (!value) {
  //     return true;
  //   }
  //   return ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'].includes(value.type);
  // }),
  photo: yup.mixed()
  // .test('fileSize', 'Image size is too large', (value) => {
  //   if (!value) {
  //     return true;
  //   }
  //   return value.size <= 1024000;
  // })
  // .test('fileType', 'Invalid file type', (value) => {
  //   if (!value) {
  //     return true;
  //   }
  //   return ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'].includes(value.type);
  // })
});
