import * as yup from 'yup';

export const registerValidationScehma = yup.object({
  fullname: yup.string().required('Name is required'),
  email: yup.string().email('Enter a valid email').required('Email is required'),
  mobile: yup
    .string()
    .required()
    .matches(/^[0-9]+$/, 'Must be only digits')
    .min(9, 'Must be minimal 10 digits!')
    .max(12, 'Must be maximal 13 digits'),
  password: yup.string().min(10, 'Password should be of minimum 10 characters length').required('Password is required'),
  passwordConfirm: yup.string().oneOf([yup.ref('password'), null], 'Passwords must match'),
  agree: yup.bool().oneOf([true], 'Field must be checked')
});
