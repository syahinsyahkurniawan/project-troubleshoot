import * as yup from 'yup';

export const otpValidationSchema = yup.object({
  otp: yup
    .string()
    .required()
    .matches(/^[0-9]+$/, 'Must be only digits')
    .min(4, 'Otp is four digits long!')
    .max(4, 'Otp is four digits long!')
});
