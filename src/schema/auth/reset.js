import * as yup from 'yup';

export const resetValidationScehma = yup.object().shape({
  password: yup.string().min(10, 'Password should be of minimum 10 characters length').required('Password is required'),
  passwordConfirm: yup.string().oneOf([yup.ref('password')], 'Passwords must match')
});
