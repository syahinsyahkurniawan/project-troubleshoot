import { mainAPI } from './config';
import { axiosx } from 'utils/api/axiosx';

const apiUrl = mainAPI;
export const getLandingData = async () => {
  const res = await axiosx(false).get(`${apiUrl}/landing`);

  return res.data;
};
