import { mainAPI } from '../config';
import { axiosx } from 'utils/api/axiosx';

const apiUrl = mainAPI;
export const getProfilesVerification = async () => {
  const res = await axiosx(true).get(`${apiUrl}/profiles`);
  return res.data;
};

export const getDetailProfilesVerification = async (uuid) => {
  const res = await axiosx(true).get(`${apiUrl}/profiles/${uuid}`);

  return res.data;
};

export const getProfilesVerificationDetail = async (id) => {
  const res = await axiosx(true).get(`${apiUrl}profiles/${id}`);

  return res.data;
};

export const postVerificationUser = async ({ uuid, data }) => {
  const res = await axiosx(true).patch(`${apiUrl}/profiles/verify/${uuid}`, data);

  return res.data;
};
