import { mainAPI } from './config';
import { axiosx } from 'utils/api/axiosx';

const apiUrl = mainAPI;
export const getProvinceData = async () => {
  const res = await axiosx(true).get(`${apiUrl}provinsi`);
  return res.data;
};

export const getRegencyData = async (data) => {
  console.log('KTP getRegencyData', data);
  const res = await axiosx(true).get(`${apiUrl}regencies?provinsiId=${data.provinsiId}&search=${data.keyword}`);
  return res.data;
};

export const getWilayahData = async (data) => {
  const res = await axiosx(true).get(`${apiUrl}wilayah?provinsiId=${data.regencyId}&search=${data.keyword}`);
  return res.data;
};
