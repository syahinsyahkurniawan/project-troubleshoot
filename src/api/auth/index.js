import { mainAPI } from '../config';
import { axiosx } from 'utils/api/axiosx';

const apiUrl = mainAPI;

export const postLogin = async (data) => {
  const res = await axiosx(false).post(`${apiUrl}/auth/login`, data);
  return res.data;
};

export const postRegistration = async (data) => {
  const res = await axiosx(false).post(`${apiUrl}/auth/register`, data);
  return res.data;
};

export const verifyOtp = async (data) => {
  const res = await axiosx(false).post(`${apiUrl}/auth/verify/otp`, data);
  return res.data;
};

export const resendOtp = async (data) => {
  const res = await axiosx(false).post(`${apiUrl}/auth/send/otp`, data);
  return res.data;
};

export const getProfile = async () => {
  const res = await axiosx(true).get(`${apiUrl}/auth/me`);
  return res.data;
};
export const postForgot = async (data) => {
  const res = await axiosx(false).post(`${apiUrl}/auth/password/forgot`, data);
  return res.data;
};
export const postReset = async (data) => {
  const res = await axiosx(false).post(`${apiUrl}/auth/password/reset`, data);
  return res.data;
};

export const insertProfile = async (data) => {
  const res = await axiosx(true).post(`${apiUrl}profiles`, data.payload);
  return res;
};
